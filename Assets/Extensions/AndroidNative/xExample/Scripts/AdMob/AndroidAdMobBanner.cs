﻿////////////////////////////////////////////////////////////////////////////////
//  
// @module Android Native Plugin for Unity3D 
// @author Osipov Stanislav (Stan's Assets) 
// @support stans.assets@gmail.com 
//
////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using System.Collections.Generic;



//Attach the script to the empty gameobject on your sceneS
public class AndroidAdMobBanner : MonoBehaviour {


	public string BannersUnityId;
	public GADBannerSize size = GADBannerSize.SMART_BANNER;
	public TextAnchor anchor = TextAnchor.LowerCenter;



	private static Dictionary<string, GoogleMobileAdBanner> _refisterdBanners = null;
	private static GameObject modelArsakidsBanner;

	// --------------------------------------
	// Unity Events
	// --------------------------------------
	
	void Awake() {
		if(AndroidAdMobController.instance.IsInited) {
			if(!AndroidAdMobController.instance.BannersUunitId.Equals(BannersUnityId)) {
				AndroidAdMobController.instance.SetBannersUnitID(BannersUnityId);
			} 
		} else {
			AndroidAdMobController.instance.Init(BannersUnityId);
			AndroidAdMobController.instance.TagForChildDirectedTreatment(true);
		}
		
		if (modelArsakidsBanner == null) {
			modelArsakidsBanner = Resources.Load<GameObject>("Prefabs/Canvas Banner");
		}
	}

	void Start() {
		ShowBanner();
	}

	void OnDestroy() {
		HideBanner();
	}


	// --------------------------------------
	// PUBLIC METHODS
	// --------------------------------------

	public void ShowBanner() {
		if (DataManager.isAdsRemoved) {
			return;
		}

		GoogleMobileAdBanner banner;
		if(registerdBanners.ContainsKey(sceneBannerId)) {
			banner = registerdBanners[sceneBannerId];
			
			if (!banner.IsLoaded) {
				banner = AndroidAdMobController.instance.CreateAdBanner(anchor, size);
				banner.OnFailedLoadingAction += OnFailedLoading;
				
				registerdBanners[sceneBannerId] = banner;
			}
		}  else {
			banner = AndroidAdMobController.instance.CreateAdBanner(anchor, size);
			registerdBanners.Add(sceneBannerId, banner);
		}

		if(banner.IsLoaded && !banner.IsOnScreen) {
			banner.Show();
		}
	}

	public void HideBanner() {
		if(registerdBanners.ContainsKey(sceneBannerId)) {
			GoogleMobileAdBanner banner = registerdBanners[sceneBannerId];
			if(banner.IsLoaded) {
				if(banner.IsOnScreen) {
					banner.Hide();
				}
			} else {
				banner.ShowOnLoad = false;
			}
		}
	}
	
	private static void OnFailedLoading(GoogleMobileAdBanner banner) {
		Instantiate (modelArsakidsBanner);
	}

	// --------------------------------------
	// GET / SET
	// --------------------------------------


	public static Dictionary<string, GoogleMobileAdBanner> registerdBanners {
		get {
			if(_refisterdBanners == null) {
				_refisterdBanners = new Dictionary<string, GoogleMobileAdBanner>();
			}

			return _refisterdBanners;
		}
	}

	public string sceneBannerId {
		get {
			return Application.loadedLevelName + "_" + this.gameObject.name;
		}
	}

	
}

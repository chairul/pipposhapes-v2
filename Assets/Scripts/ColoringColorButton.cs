﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColoringColorButton : MonoBehaviour {
	public ColoringDrawingController controller;
	public Color thisColor;
	public GameObject currentColor;
	public GameObject newColor;

	//drag
	public bool isDragged = false;
	bool beginDrag = false;
	float dragDelay = 0.1f;

	void CreateNewShape () {
		newColor = Instantiate (currentColor, controller.colorsPanel.transform); 
		newColor.transform.GetChild(0).gameObject.GetComponent<Image> ().color = thisColor;
	}

	void Update () {
		if (beginDrag) {
			dragDelay -= Time.deltaTime;
			if (dragDelay <= 0) {
				isDragged = true;
				CreateNewShape ();
				Vector3 point = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				point.z = -1;
				newColor.transform.position = point;
				dragDelay = 0.1f;
				beginDrag = false;
			}
		}
	}

	void OnMouseDown () {
		isDragged = false;
		beginDrag = true;	
		currentColor.transform.GetChild(0).gameObject.GetComponent<Image> ().color = thisColor;
		controller.currentColor = thisColor;
	}

	void OnMouseDrag () {
		if (isDragged) {
			Vector3 point = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			point.z = -1;
			newColor.transform.position = point;
			Cursor.visible = false;
		}
	}

	void OnMouseUp () {
		beginDrag = false;
		dragDelay = 0.1f;
		if (!isDragged) {
			currentColor.transform.GetChild(0).gameObject.GetComponent<Image> ().color = thisColor;
			controller.currentColor = thisColor;
		}
		else { 
			if (newColor.GetComponent<ColoringColor> ().isOnShape) {
				newColor.GetComponent<ColoringColor> ().shapeObject.GetComponent<SpriteRenderer> ().color = thisColor;
			}
			Destroy (newColor);
		}
		Cursor.visible = true;
	}
}

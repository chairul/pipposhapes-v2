﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class PointClickController : MonoBehaviour {
	public GameObject panelsLayout;
	public List<GameObject> panels = new List<GameObject> ();


	float panelWidth = 1906;
	float totalPanelHeight = 0;

	//closed panel
	float closedPanelHeight = 490;
	float closedPanelSpacing = -200;

	//open panel
	float[] openPanelHeights;
	float openPanelSpacing = -100;

	[Header("Board")]
	public GameObject board;
	public Image shapeImage;
	public Text shapeName;
	JSONNode load;
	public Image[] similarShapes;
	public int currentShapeCode;
	public int[] similarRules;
	public GameObject similarRuleLayout;
	public GameObject similarRuleSample;
	float similarRuleLayoutHeight = 190;
	float similarRuleLayoutSpacing = 30;
	float similarRuleShapeSize = 170;
	public List<GameObject> shapeSimilarRule = new List<GameObject> ();
	//details
	public bool isBoardDetailed = false;
	public GameObject detailBoard;


	void Start () {
		//get open panel height
		openPanelHeights = new float[panels.Count];
		for (int i = 0; i < panels.Count; i++) {
			openPanelHeights[i] = panels [i].GetComponent<RectTransform> ().rect.height;
		}
		BuildOpenLayout ();


		string cardsData = Resources.Load<TextAsset> ("Data/flashcards").ToString ();
		load = JSON.Parse (cardsData);
		board.SetActive (false);
	}

	public void BuildClosedLayout () {
		totalPanelHeight = 0;
		for (int i = 0; i < panels.Count; i++) {
			//panels [i].GetComponent<RectTransform> ().sizeDelta = new Vector2 (panels [i].GetComponent<RectTransform> ().rect.width, closedPanelHeight);
			totalPanelHeight += panels [i].GetComponent<RectTransform> ().rect.height;

		}
		panelsLayout.GetComponent<VerticalLayoutGroup> ().spacing = closedPanelSpacing;
		panelsLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 (panelWidth, totalPanelHeight + (panels.Count * closedPanelSpacing));
		panelsLayout.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -panelsLayout.GetComponent<RectTransform> ().sizeDelta.y);
	}

	public void BuildOpenLayout () {
		totalPanelHeight = 0;
		for (int i = 0; i < panels.Count; i++) {
			//panels [i].GetComponent<RectTransform> ().sizeDelta = new Vector2 (panels [i].GetComponent<RectTransform> ().rect.width, openPanelHeights[i]);
			totalPanelHeight += panels [i].GetComponent<RectTransform> ().rect.height;
		}
		panelsLayout.GetComponent<VerticalLayoutGroup> ().spacing = openPanelSpacing;
		panelsLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 (panelWidth, totalPanelHeight + (panels.Count * openPanelSpacing));
		panelsLayout.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -panelsLayout.GetComponent<RectTransform> ().sizeDelta.y);
	}


	public void BuildBoard (int shapeCode) {
		currentShapeCode = shapeCode;
		board.SetActive (true);
		shapeImage.sprite = Resources.Load<Sprite> ("Shapes/" + shapeCode);
		shapeName.text = load [shapeCode] ["name"];
		List<int> similarShapesCode = new List<int> ();
		for (int i = 0; i < load.Count; i++) {
			if (load [shapeCode] ["groupCode"].AsInt == load [i] ["groupCode"].AsInt && i != shapeCode){
				similarShapesCode.Add (i);
			}
		}
		for (int i = 0; i < similarShapes.Length; i++) {
			similarShapes [i].sprite = null;
		}
		for (int i = 0; i < similarShapesCode.Count; i++) {
			if (i < similarShapes.Length) {
				similarShapes [i].sprite = Resources.Load<Sprite> ("Shapes/" + similarShapesCode [i]);
			}
		}
		for (int i = 0; i < similarShapes.Length; i++) {
			if (similarShapes [i].sprite == null) {
				similarShapes [i].gameObject.SetActive (false);
			} else {
				similarShapes [i].gameObject.SetActive (true);
			}
		}

		//Similar Ruled Shapes
		for (int i = 0; i < shapeSimilarRule.Count; i++) {
			Destroy (shapeSimilarRule [i]);
		}
		shapeSimilarRule.Clear ();
		if (shapeCode != 0 && shapeCode != 1 && shapeCode != 2) {
			for (int i = 0; i < similarRules.Length; i++) {
				if (load [similarRules [i]] ["groupCode"].AsInt != load [shapeCode] ["groupCode"].AsInt) {
					GameObject newShape = Instantiate (similarRuleSample, similarRuleLayout.transform);
					newShape.GetComponent<Image>().sprite = Resources.Load<Sprite> ("Shapes/" + similarRules [i]);
					newShape.SetActive (true);
					shapeSimilarRule.Add (newShape);
				}
			}
		}
		similarRuleLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 ((shapeSimilarRule.Count * similarRuleShapeSize) + (shapeSimilarRule.Count * similarRuleLayoutSpacing), similarRuleLayoutHeight);
		similarRuleLayout.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (similarRuleLayout.GetComponent<RectTransform> ().sizeDelta.x, 0);

		isBoardDetailed = false;
		detailBoard.SetActive (false);
	}

	public void NextBoard () {
		currentShapeCode = Modulo (currentShapeCode + 1, load.Count);
		BuildBoard (currentShapeCode);
	}

	public void PrevBoard () {
		currentShapeCode = Modulo (currentShapeCode - 1, load.Count);
		BuildBoard (currentShapeCode);
	}

	public void ReadBoard () {
		board.GetComponent<AudioSource> ().clip = Resources.Load<AudioClip> ("Shapes/Clip/" + currentShapeCode);
		board.GetComponent<AudioSource> ().Play ();
	}

	public void DetailBoard () {
		if (isBoardDetailed) {
			detailBoard.SetActive (false);
			isBoardDetailed = false;
		} else {
			detailBoard.SetActive (true);
			isBoardDetailed = true;
		}
	}

	public void MoreSimilarRuleShapes () {
		similarRuleLayout.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (similarRuleLayout.GetComponent<RectTransform> ().anchoredPosition.x - 200, 0);
	}


	public void CloseBoard () {
		board.SetActive (false);
	}

	int Modulo (int a, int b) {
		return (a % b + b) % b;
	}
}

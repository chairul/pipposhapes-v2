﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class ColoringShape : MonoBehaviour {
	public ColoringDrawingController controller;
	public bool isDragable = true;
	public bool isOutOfBound = false;
	public bool isOnCanvas = false;
	Vector2 thisPosition;

	void OnMouseDown () {
		thisPosition = this.transform.position;
		if (controller.isInColorMode) {
			this.GetComponent<SpriteRenderer> ().color = controller.currentColor;
		}
		//Transform Mode
		if (!controller.isInColorMode && !controller.isInPencilMode) {
			controller.activeShape = this.gameObject;
		}
	}

	void OnMouseDrag () {
		if (isDragable && (!controller.isInColorMode && !controller.isInPencilMode)) {
			Vector3 point = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			point.z = -1;
			gameObject.transform.position = point;
			Cursor.visible = false;
		}
	}
		
	void OnMouseUp () {
		Cursor.visible = true;
		if (isOutOfBound) {
			this.transform.position = thisPosition;
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == "CanvasBound") {
			isOutOfBound = true;
		}
		if (other.gameObject.tag == "Canvas") {
			isOnCanvas = true;
		}
	}
	void OnTriggerExit2D (Collider2D other) {
		if (other.gameObject.tag == "CanvasBound") {
			isOutOfBound = false;
		}
		if (other.gameObject.tag == "Canvas") {
			isOnCanvas = false;
		}
	}

	void Update () {
		if (this.gameObject != controller.activeShape) {
			for (int i = 0; i < this.transform.childCount; i++) {
				this.transform.GetChild (i).gameObject.SetActive (false);
			}
			this.GetComponent<LeanRotate> ().enabled = false;
			this.GetComponent<LeanScale> ().enabled = false;
		} else {
			for (int i = 0; i < this.transform.childCount; i++) {
				this.transform.GetChild (i).gameObject.SetActive (true);
			}
			this.GetComponent<LeanRotate> ().enabled = true;
			this.GetComponent<LeanScale> ().enabled = true;
		}
		if (LeanTouch.Fingers.Count >= 2) {
			isDragable = false;
		} else {
			isDragable = true;
		}
	}
}

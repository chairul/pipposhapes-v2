﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColoringPencilButton : MonoBehaviour {
	public ColoringDrawingController controller;
	public Color thisPencil;
	public GameObject currentPencil;

	void OnMouseUp () {
		currentPencil.transform.GetChild(0).gameObject.GetComponent<Image> ().color = thisPencil;
		controller.currentPencil = thisPencil;
	}
}

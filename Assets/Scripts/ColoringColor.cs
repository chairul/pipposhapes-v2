﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColoringColor : MonoBehaviour {
	public bool isOnShape = false;
	public GameObject shapeObject;
	public Color shapeObjectColor;

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.gameObject.tag == "Stickable") {
			isOnShape = true;
			shapeObject = other.gameObject;
			shapeObjectColor = other.gameObject.GetComponent<SpriteRenderer> ().color;
		}
	}
	void OnTriggerExit2D (Collider2D other) {
		if (other.gameObject.gameObject.tag == "Stickable") {
			isOnShape = false;
		}
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointClickShapes : MonoBehaviour {
	public PointClickController controller;
	public int shapeCode = 0;

	void Update () {
		shapeCode = int.Parse (this.GetComponent<Image> ().sprite.name);
	}

	public void Click () {
		controller.BuildBoard (shapeCode);
	}
		
}

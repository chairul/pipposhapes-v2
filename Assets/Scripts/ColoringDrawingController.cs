﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class ColoringDrawingController : MonoBehaviour {
	public ColoringGameController gameController;

	[Header("Panels")]
	public GameObject buttonsPanel;
	public GameObject shapesPanel;
	public GameObject colorsPanel;
	public GameObject pencilsPanel;
	public GameObject backButton;
	public GameObject backToMainButton;
	public SpriteRenderer background;

	[Header("Shapes Panel")]
	public List<GameObject> shapesObject = new List<GameObject>();
	public GameObject shapeSample;
	public GameObject shapeLayout;		//Object with Layout Group
	const float shapeLayoutWidth = 200;
	const float shapeButtonsSize = 150;
	const float shapeButtonSpacing = 50;
	public GameObject[] shapesSummon;
	public int currentLayer = 3;
	public List<GameObject> shapeOnCanvas = new List<GameObject> ();
	public GameObject activeShape;

	[Header("Colors Panel")]
	public List<Color> colors = new List<Color>();
	public GameObject colorSample;
	public GameObject colorLayout;		//Object with Layout Group
	const float colorLayoutWidth = 200;
	const float colorButtonsSize = 100;
	const float colorButtonSpacing = 40;
	public Color currentColor = Color.white;
	public bool isInColorMode = false;

	[Header("Pencils Panel")]
	public List<Color> pencils = new List<Color> ();
	public GameObject pencilSample;
	public GameObject pencilLayout;		//Object with Layout Group
	const float pencilLayoutWidth = 200;
	const float penciluttonsSize = 100;
	const float pencilButtonSpacing = 40;
	public Color currentPencil = Color.white;
	public bool isInPencilMode = false;
	public GameObject canvas;

	[Header("Eraser")]
	public bool isInEraserMode = false;
	public GameObject eraser;

	[Header("Done")]
	public bool isDoneDrawing = false;
	float delay = 0.5f;

	void GetShapes () {
		Sprite[] shapes = Resources.LoadAll<Sprite> ("ShapesColoring");
		shapesObject.Clear ();
		for (int i = 0; i < shapes.Length; i++) {
			GameObject newShapeButton = Instantiate (shapeSample, shapeLayout.transform);
			newShapeButton.GetComponent<Image> ().sprite = shapes [i];
			newShapeButton.GetComponent<ColoringShapeButton> ().controller = this;
			newShapeButton.GetComponent<ColoringShapeButton> ().shapeCode = i;
			newShapeButton.SetActive (true);
			shapesObject.Add (newShapeButton);
		}
		shapeLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 (shapeLayoutWidth, (shapesObject.Count * shapeButtonsSize) + (shapesObject.Count * shapeButtonSpacing));
		shapeLayout.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -shapeLayout.GetComponent<RectTransform> ().sizeDelta.y);

		shapesSummon = new GameObject[shapes.Length];
		for (int i = 0; i < shapes.Length; i++) {
			shapesSummon [i] = Resources.Load ("Prefabs/ShapeColoring/" + shapes [i].name) as GameObject;
		}
	}

	void GetColors () {
		string colorsData = Resources.Load<TextAsset> ("Data/colors").ToString();
		JSONNode load = JSON.Parse (colorsData);
		for (int i = 0; i < load.Count; i++) {
			Color temp = new Color ();
			ColorUtility.TryParseHtmlString (load [i] ["hex"], out temp);
			colors.Add (temp);
		}

		for (int i = 0; i < colors.Count; i++) {
			GameObject newColorButton = Instantiate (colorSample, colorLayout.transform);
			newColorButton.GetComponent<Image> ().color = colors [i];
			newColorButton.GetComponent<ColoringColorButton> ().controller = this;
			newColorButton.GetComponent<ColoringColorButton> ().thisColor = colors [i];
			newColorButton.SetActive (true);
		}
		colorLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 (colorLayoutWidth, (colors.Count * colorButtonsSize) + (colors.Count * colorButtonSpacing));
		colorLayout.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -colorLayout.GetComponent<RectTransform> ().sizeDelta.y);
		canvas.GetComponent<ColoringPencil> ().controller = this;
	}

	void GetPencils () {
		pencils = colors;

		for (int i = 0; i < colors.Count; i++) {
			GameObject newPencilButton = Instantiate (pencilSample, pencilLayout.transform);
			newPencilButton.GetComponent<Image> ().color = colors [i];
			newPencilButton.GetComponent<ColoringPencilButton> ().controller = this;
			newPencilButton.GetComponent<ColoringPencilButton> ().thisPencil = pencils [i];
			newPencilButton.SetActive (true);
		}
		pencilLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 (pencilLayoutWidth, (pencils.Count * penciluttonsSize) + (pencils.Count * pencilButtonSpacing));
		pencilLayout.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -pencilLayout.GetComponent<RectTransform> ().sizeDelta.y);
	}

	void Start () {
		currentLayer = 3;
		GetShapes ();
		GetColors ();
		GetPencils ();
		eraser.GetComponent<ColoringEraser> ().controller = this;
		buttonsPanel.SetActive (true);
		shapesPanel.SetActive (false);
		colorsPanel.SetActive (false);
		pencilsPanel.SetActive (false);
		backButton.SetActive (false);
		backToMainButton.SetActive (true);
		gameController = this.GetComponent<ColoringGameController> ();
	}

	public void OpenShapes () {
		activeShape = null;
		buttonsPanel.SetActive (false);
		shapesPanel.SetActive (true);
		backButton.SetActive (true);
		backToMainButton.SetActive (false);
	}

	public void OpenColors () {
		isInColorMode = true;
		activeShape = null;
		buttonsPanel.SetActive (false);
		colorsPanel.SetActive (true);
		backButton.SetActive (true);
		backToMainButton.SetActive (false);
	}

	public void OpenPencils () {
		isInPencilMode = true;
		activeShape = null;
		buttonsPanel.SetActive (false);
		pencilsPanel.SetActive (true);
		backButton.SetActive (true);
		backToMainButton.SetActive (false);
	}

	public void OpenEraser () {
		isInEraserMode = true;
		activeShape = null;
		buttonsPanel.SetActive (false);
		backButton.SetActive (true);
		backToMainButton.SetActive (false);
	}

	public void BackPanel () {
		isInColorMode = false;
		isInPencilMode = false;
		isInEraserMode = false;
		buttonsPanel.SetActive (true);
		shapesPanel.SetActive (false);
		colorsPanel.SetActive (false);
		pencilsPanel.SetActive (false);
		backButton.SetActive (false);
		backToMainButton.SetActive (true);
		activeShape = null;
	}

	public void BackTo () {
		gameController.lines = GameObject.FindGameObjectsWithTag ("Line");
		if (gameController.lines.Length > 0) {
			for (int i = 0; i < gameController.lines.Length; i++) {
				gameController.lines [i].SetActive (false);
			}
		}
		gameController.backgroundSelectScreen.SetActive (true);
		gameController.drawingScreen.SetActive (false);
	}

	public void DoneDrawing () {
		activeShape = null;
		delay = 0.5f;
		isDoneDrawing = true;
	}

	public void MoreItems (GameObject scrollLayout) {
		scrollLayout.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, scrollLayout.GetComponent<RectTransform> ().anchoredPosition.y + 200);
	}

	void Update () {
		if (isInEraserMode) {
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
			if (hit.collider != null) {
				if (hit.collider.gameObject.tag == "Stickable") {
					if (Input.GetMouseButtonDown (0)) {
						Destroy (hit.collider.gameObject);
					}
				}
				if (hit.collider.gameObject.tag == "Line") {
					if (Input.GetMouseButtonDown (0)) {
						Destroy (hit.collider.gameObject);
					}
				}
			}
			if (Input.GetMouseButton (0)) {
				Vector3 point = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				point.z = -1;
				//eraser.transform.position = point;
				//eraser.SetActive (true);
				Cursor.visible = false;
			}
			if (Input.GetMouseButtonUp (0)) {
				eraser.SetActive (false);
				Cursor.visible = true;
			}
		} else {
			eraser.SetActive (false);
			Cursor.visible = true;
		}

		if (isDoneDrawing) {
			delay -= Time.deltaTime;
			if (delay <= 0) {
				GameObject[] lines = GameObject.FindGameObjectsWithTag ("Line");
				for (int i = 0; i < lines.Length; i++) {
					Destroy (lines [i].gameObject);
				}

				Texture2D tex = new Texture2D(gameController.shot.width, gameController.shot.height, TextureFormat.RGB24, false);
				RenderTexture.active = gameController.shot;
				tex.ReadPixels(new Rect(0, 0, gameController.shot.width, gameController.shot.height), 0, 0);
				tex.Apply();
				var bytes = tex.EncodeToPNG();
				Destroy(tex);
				System.IO.File.WriteAllBytes (Application.persistentDataPath + " " + this.GetComponent<ColoringBackgroundController> ().backgroundIndex + ".png", bytes);


				gameController.background.sprite = gameController.backgrounds [1];
				gameController.drawingScreen.SetActive (false);
				gameController.galleryScreen.SetActive (true);
				this.GetComponent<ColoringGalleryController> ().StartGallery ();
				isDoneDrawing = false;
			}
		}
	}
}

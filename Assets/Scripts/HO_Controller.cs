﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HO_Controller : MonoBehaviour {
	public GameObject[] boxes;
	[Header("Shape Layout Props")]
	public GameObject placeLayout;
	public GameObject placeSample;
	public List<GameObject> shapePlaces = new List<GameObject> ();
	public float placeLayoutWidth = 325;
	public float shapeSize = 300;
	public float shapeSpacing = 50;

	//indicator
	public GameObject indicator;
	public ChallengeFeedback feedback;

	void Start () {
		GetBoxes ();
		CreateBox (Random.Range (0, boxes.Length));
	}

	void GetBoxes () {
		boxes = Resources.LoadAll<GameObject> ("Prefabs/HO_Boxes");
	}

	void CreateBox (int boxCode) {
		GameObject newBox = Instantiate (boxes [boxCode]);
		newBox.GetComponent<HO_Box> ().controller = this;
	}
}

﻿using UnityEngine;
using System.Collections;

public class InAppManager {
	
	private static string REMOVE_ADS = "shape_ads_removal";
//	private static string REMOVE_ADS = "android.test.purchased";
	
	public static void Initialize() {
		#if UNITY_ANDROID
		AndroidInAppPurchaseManager.Instance.AddProduct(REMOVE_ADS);
		AndroidInAppPurchaseManager.ActionProductPurchased += HandleOnProductPurchased;
		AndroidInAppPurchaseManager.ActionBillingSetupFinished += OnBillingConnected;
		AndroidInAppPurchaseManager.Instance.LoadStore();
		#endif
	}
	
	public static void PurchaseItem(string code) {
		#if UNITY_ANDROID
		AndroidInAppPurchaseManager.Instance.Purchase (REMOVE_ADS);
		#endif
	}
	
	#if UNITY_ANDROID
	static void HandleOnProductPurchased(BillingResult result) {
		if (result.isSuccess) {
			ProcessingPurchasedProduct(result.purchase);
			MessageManager.DisplayMessage(LocalizedContent.GetLocaleText("iap_success"));
		} else {
			MessageManager.DisplayMessage(LocalizedContent.GetLocaleText("iap_failed") + "\n\n" + result.message);
		}
	}
	
	static void ProcessingPurchasedProduct(GooglePurchaseTemplate purchase) {
		if (purchase.SKU == REMOVE_ADS) {
			DataManager.isAdsRemoved = true;
			DataManager.SaveData();
			
			GameAnalyticsSDK.GameAnalytics.NewBusinessEvent("IDR", 3000, "Ads Remove", "ads_removal", "Shop");
		}
	}
	
	static void OnBillingConnected(BillingResult result) {
		AndroidInAppPurchaseManager.ActionBillingSetupFinished -= OnBillingConnected;
		
		if (result.isSuccess) {
			//Store connection is Successful. Next we loading product and customer purchasing details
			AndroidInAppPurchaseManager.instance.retrieveProducDetails();
			AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetrieveProductsFinised;
			//			MessageManager.DisplayMessage ("Billing Connected");
		} else {
			//			MessageManager.DisplayMessage ("Billing Failed to Connect\n\n" + result.message);
		}
	}
	
	private static void OnRetrieveProductsFinised(BillingResult result) {
		if(result.isSuccess) {
			AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetrieveProductsFinised;
			UpdateStoreData();
		} else {
			Debug.LogWarning("Connection Responce\n" + result.response.ToString() + "\n" + result.message);
		}
	}
	
	private static void UpdateStoreData() {
		//		foreach(GoogleProductTemplate p in AndroidInAppPurchaseManager.instance.Inventory.Products) {
		//			Debug.Log("Loaded product: " + p.Title);
		//		}
		
		if(AndroidInAppPurchaseManager.instance.Inventory.IsProductPurchased(REMOVE_ADS)) {
			if (!DataManager.isAdsRemoved) {
				DataManager.isAdsRemoved = true;
				DataManager.SaveData();
			}
		}
	}
	#endif
}

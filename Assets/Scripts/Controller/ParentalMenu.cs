﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ParentalMenu : SceneManager {
	
	public static string ORDER_IAP = "iap";
	public static string specificOrder = "";
	
	public GameObject menuContainer;
	public GameObject parentalChallenge;
	public GameObject buttonIap;
	public Text textOrder;
	
	private string[] challengeList = {"D", "I", "K", "S"};
	private string[] challengeOrder;
	private int clickIndex;
	
	// Use this for initialization
	void Start () {
		SetCallbackScreen ("Mainmenu");
		
		for (int i = 0; i < parentalChallenge.transform.childCount; i++) {
			parentalChallenge.transform.GetChild(i).GetComponent<ClickableObject>().OnClick += HandleChallengeObjectOnClicked;
		}
		
		CreateChallenge ();
	}
	
	protected override void OnUpdateCalled () {
		
	}
	
	void HandleChallengeObjectOnClicked(GameObject theObject) {
		if (theObject.GetComponent<ParentalChallengeObject> ().objectType.ToString () == challengeOrder [clickIndex]) {
			++clickIndex;
			
			if (clickIndex == challengeOrder.Length) {
				parentalChallenge.transform.parent.gameObject.SetActive (false);
				menuContainer.SetActive (true);
				
				if (specificOrder != "") {
					if (specificOrder == ORDER_IAP) {
						buttonIap.GetComponent<PurchaseInAppButton>().OnActivate();
					}
					
					specificOrder = "";
				}
			}
		} else {
			CreateChallenge();
			MessageManager.DisplayMessage(LocalizedContent.GetLocaleText("wrong_parent"));
		}
	}
	
	void CreateChallenge() {
		clickIndex = 0;
		challengeOrder = new string[3];
		
		string challengeOrderText = "";
		
		for (int i = 0; i < challengeOrder.Length; i++) {
			challengeOrder[i] = challengeList[Random.Range(0, challengeList.Length)];
			
			challengeOrderText += challengeOrder[i];
			
			if (i < challengeOrder.Length - 1) {
				challengeOrderText += " ";
			}
		}
		
		textOrder.text = challengeOrderText;
	}
}

﻿using UnityEngine;
using System.Collections;

public class AdsManager {
	
	public static AdsManager instance;

	public static void Init() {
		instance = new AdsManager ();
	}

	#if UNITY_ANDROID
	private const string ADS_BANNER_ID = "ca-app-pub-1046748983033415/1542970687";
	private const string ADS_INTERSTITIAL_ID = "ca-app-pub-1046748983033415/3019703885";
//	private const string ADS_INTERSTITIAL_ID = "ca-app-pub-6101605888755494/3301497967";
	#endif

	private int counter;
	private bool isAdsReady;
	private GameObject modelReminder;

	public AdsManager() {
		modelReminder = Resources.Load<GameObject> ("Prefabs/Canvas Reminder");

		#if UNITY_ANDROID
		AndroidAdMobController.instance.Init(ADS_BANNER_ID, ADS_INTERSTITIAL_ID);
		AndroidAdMobController.instance.TagForChildDirectedTreatment(true);

		AndroidAdMobController.instance.OnInterstitialLoaded += HandleOnInterstitialLoaded;
		AndroidAdMobController.instance.OnInterstitialFailedLoading += HandleOnInterstitialFailedLoading;
		AndroidAdMobController.instance.OnInterstitialOpened += HandleOnInterstitialOpened;

//		AndroidAdMobController.instance.LoadInterstitialAd();
		#endif
	}

	void HandleOnInterstitialLoaded() {
		isAdsReady = true;
//		MessageManager.DisplayMessage ("Ads Loaded");
	}

	void HandleOnInterstitialFailedLoading() {
		isAdsReady = false;
//		MessageManager.DisplayMessage ("Ads Failed Loading");
	}

	void HandleOnInterstitialOpened() {
		isAdsReady = false;
//		AndroidAdMobController.instance.LoadInterstitialAd ();
	}

	public void TryToShowAds() {
		if (DataManager.isAdsRemoved) {
			return;
		}
		
		if (counter > 0 && counter % 3 == 0) {
			if (isAdsReady) {
//				AndroidAdMobController.instance.ShowInterstitialAd();
			} else {
//				AndroidAdMobController.instance.LoadInterstitialAd();
			}
		} else {
			if (counter % 5 == 0) {
				GameObject reminder = (GameObject) GameObject.Instantiate(modelReminder);
			}
			
			if (!isAdsReady) {
				AndroidAdMobController.instance.LoadInterstitialAd();
			}
		}
		
		++counter;
	}

	public void DisplayBanner() {
		GoogleMobileAdBanner banner;

		banner = AndroidAdMobController.Instance.CreateAdBanner(TextAnchor.LowerCenter, GADBannerSize.BANNER);
	}
}

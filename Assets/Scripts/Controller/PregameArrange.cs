﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PregameArrange : MonoBehaviour {

	private readonly float pictureWidth = 224f;
	private readonly float pictureHeight = 224f;
	private readonly int numOfColumn = 4;

	public RectTransform container;
	public Transform pictureModel;

	// Use this for initialization
	void Start () {
		container.sizeDelta = new Vector2 (numOfColumn * pictureWidth, ((GameArrangeManager.pictureNames.Length / numOfColumn) + 1) * pictureHeight);

		for (int i = 0; i < GameArrangeManager.pictureNames.Length; i++) {
			Transform newPicture = (Transform) Instantiate(pictureModel);

			newPicture.SetParent(container);
			newPicture.localPosition = new Vector3(newPicture.position.x + (i % numOfColumn) * pictureWidth, newPicture.position.y - (i / numOfColumn) * pictureHeight);
			newPicture.localScale = new Vector3(0.3f, 0.3f);
			newPicture.GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/Arranged Pictures/gc_picture_" + GameArrangeManager.pictureNames[i]);
			newPicture.GetComponent<ClickableObject>().OnClick += HandleOnPictureClicked;
			newPicture.name = GameArrangeManager.pictureNames[i];

			newPicture.gameObject.SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void HandleOnPictureClicked(GameObject theObject) {
		GameArrangeManager.selectedPictureName = theObject.name;
		Application.LoadLevel ("Gameplay Arrange");
	}
}

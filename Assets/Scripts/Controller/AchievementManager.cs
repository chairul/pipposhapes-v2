﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class AchievementManager {

	/* 
	 * Class Achievement
	 * Data Structure for Achievement
	 * It's made as a class to make editing progress easier
	 */
	public class Achievement {
		public string id;
		public string title;
		public int target;
		public int progress;
		public bool isCompleted { get {return target == progress;}}

		public Achievement() {

		}
	}

	public static Dictionary<string, Achievement> achievementList;

	/*
	 * Function LoadData
	 * Read achievement data from resources
	 */
	public static void LoadData() {
		string achievementDataString = Resources.Load<TextAsset> ("Data/achievement").text;
		var achievementData = JSONNode.Parse (achievementDataString);

		achievementList = new Dictionary<string, Achievement> ();

		for (int i = 0; i < achievementData.AsArray.Count; i++) {
			Achievement achievementEntry = new Achievement();
			achievementEntry.id = achievementData[i]["id"];
			achievementEntry.title = achievementData[i]["title"];
			achievementEntry.target = achievementData[i]["target"].AsInt;

			achievementList.Add(achievementEntry.id, achievementEntry);
		}
	}

	/*
	 * Function ProgressAchievement
	 * Update progress of achievement
	 */
	public static void ProgressAchievement(string id, int value) {
		if (!achievementList [id].isCompleted) {
			achievementList[id].progress += value;
		}
	}
}

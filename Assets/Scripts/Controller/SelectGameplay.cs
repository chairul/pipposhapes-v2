﻿using UnityEngine;
using System.Collections;

public class SelectGameplay : SceneManager {

	public Animator pippoAnimator;
	public Animator picaAnimator;
	public Animator picoAnimator;
	public GameObject gameButton1;
	public GameObject gameButton2;
	public GameObject gameButton3;
	public GameObject blocker;

	private int charIndex;
	private float startTime;

	/*
	 * Initialization
	 */
	void Start() {
		/* === Set Callback Screen === */
		SetCallbackScreen ("Mainmenu");
		/* === === */

		charIndex = 0;

		pippoAnimator.GetComponent<AnimationEvent> ().OnAnimationFinished += HandleCharacterChooseAnimationFinished;
		picaAnimator.GetComponent<AnimationEvent> ().OnAnimationFinished += HandleCharacterChooseAnimationFinished;
		picoAnimator.GetComponent<AnimationEvent> ().OnAnimationFinished += HandleCharacterChooseAnimationFinished;
		
		gameButton1.GetComponent<GameSelectionButton>().OnGameplaySelected += HandleOnGameplaySelected;
		gameButton2.GetComponent<GameSelectionButton>().OnGameplaySelected += HandleOnGameplaySelected;
		gameButton3.GetComponent<GameSelectionButton>().OnGameplaySelected += HandleOnGameplaySelected;

		picaAnimator.SetTrigger ("Choose");
		
		SoundManager.PlaySFXOneShot (SoundManager.clipVOSelectGameplay);
		
		startTime = Time.time;

		AdsManager.instance.TryToShowAds ();
	}
	
	/*
	 * Customized update call
	 */
	protected override void OnUpdateCalled () {
		if (Time.time - startTime >= 0.5f && blocker.activeSelf) {
			picaAnimator.GetComponent<BoxCollider2D> ().enabled = true;
			pippoAnimator.GetComponent<BoxCollider2D> ().enabled = true;
			picoAnimator.GetComponent<BoxCollider2D> ().enabled = true;
			blocker.SetActive(false);
		}
	}
	
	void HandleOnGameplaySelected (GameObject theObject) {
		picaAnimator.GetComponent<BoxCollider2D> ().enabled = false;
		pippoAnimator.GetComponent<BoxCollider2D> ().enabled = false;
		picoAnimator.GetComponent<BoxCollider2D> ().enabled = false;
		
		if (theObject == gameButton1) {
			picaAnimator.SetTrigger ("Click");
		} else if (theObject == gameButton2) {
			pippoAnimator.SetTrigger ("Click");
		} else if (theObject == gameButton3) {
			picoAnimator.SetTrigger ("Click");
		}
	}

	void HandleCharacterChooseAnimationFinished(GameObject aObject) {
		++charIndex;

		if (charIndex == 3) {
			charIndex = 0;
		}

		if (charIndex == 0) {
			picaAnimator.SetTrigger ("Choose");
		} else if (charIndex == 1) {
			pippoAnimator.SetTrigger ("Choose");
		} else if (charIndex == 2) {
			picoAnimator.SetTrigger ("Choose");
		}
	}
}

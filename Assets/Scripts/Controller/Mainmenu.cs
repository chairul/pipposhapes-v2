﻿using UnityEngine;
using System.Collections;

public class Mainmenu : SceneManager {
	
	private static bool isVOPlayed;

	public Animator buttonPlayAnimator;
	public GameObject blocker;
	
	private float startTime;

	/*
	 * Initialization
	 */
	void Start() {
//		if (!isVOPlayed) {
			SoundManager.PlaySFXOneShot (Resources.Load<AudioClip> ("VO/vo_title_game"));
//			isVOPlayed = true;
//		}

		/* === Set Callback Screen === */
		SetCallbackScreen (QUIT);
		/* === === */
		
		DataManager.SaveData();
		
		startTime = Time.time;
	}
	
	/*
	 * Customized update call
	 */
	protected override void OnUpdateCalled () {
		if (Time.time - startTime >= 0.5f && blocker.activeSelf) {
			blocker.SetActive(false);
		}
	}

	public void FinishInitialAnimation() {
		buttonPlayAnimator.SetTrigger ("Idle");
	}

	public void SetNodePosition (int indexNode) {
		GameplaySelectorBackgroundController.startNodeIndex = indexNode;
	}
}

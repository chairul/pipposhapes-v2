﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class OtherGameAdManager : MonoBehaviour {

	struct OtherGameAdInfo {
		public string id;
		public string title;
	}

	private static List<OtherGameAdInfo> otherGameAdInfoList;

	public static void LoadInfoData() {
		var infoData = JSONNode.Parse (Resources.Load<TextAsset>("Data/other_game_list").text);
		otherGameAdInfoList = new List<OtherGameAdInfo> ();

		for (int i = 0; i < infoData.AsArray.Count; i++) {
			OtherGameAdInfo infoObject = new OtherGameAdInfo();

			infoObject.id = infoData[i]["id"];
			infoObject.title = infoData[i]["title_" + DataManager.locale.ToString()];

			otherGameAdInfoList.Add(infoObject);
		}
	}

	private const float contentWidth = 244f;
	private const float contentHeight = 233f;
	private const int numOfColumn = 4;

	public RectTransform contentHolder;
	public RectTransform contentModel;

	// Use this for initialization
	void Start () {
		Vector2 contentHolderDimension = new Vector2 (contentWidth * numOfColumn, contentHeight * ((otherGameAdInfoList.Count / numOfColumn) + 1));
		contentHolder.sizeDelta = contentHolderDimension;

		for (int i = 0; i < otherGameAdInfoList.Count; i++) {
			RectTransform newContent = (RectTransform) Instantiate(contentModel);
			newContent.SetParent(contentHolder);
			newContent.localPosition = new Vector3(contentModel.localPosition.x + (contentWidth * (i % numOfColumn)), contentModel.localPosition.y - (contentHeight * (i / numOfColumn)));
			newContent.localScale = new Vector3(1f, 1f);
			newContent.gameObject.SetActive(true);

			newContent.GetComponent<OtherGameAdContent>().Init(Resources.Load<Sprite>("Sprites/Othergame/icon_" + otherGameAdInfoList[i].id),
			                                                   otherGameAdInfoList[i].title,
			                                                   otherGameAdInfoList[i].id);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameArrangeManager : SceneManager {

	struct PicturePartInfo {
		public Vector3 rotation;
		public Vector3 scale;
		public Vector3 position;
		public float size;
		public float width;
		public float height;
	}

	public static string[] pictureNames = {"Bee", "Bird", "Butterfly", "Cat", "Chicken", "Dance", "Doe", "Fish", "Horse", "House", 
		"Littlebird", "Mill", "Motor", "Plane", "Run", "Ship", "Snake", "Squirrel"};
	public static string selectedPictureName;

	public RectTransform canvasWorld;
	public RectTransform shapeContainer;
	public ChallengeFeedback challengeFeedback;
	public GameObject blocker;
	public AudioClip clipVOInstruction;

	private string[] shapeTypeNames;
	private Dictionary<string, GameObject> shapeModels;
	private Transform picture;
	private List<int> completedPartIndex;
	private List<Transform> pictureParts;
//	private List<PicturePartInfo> picturePartInfoList;

	private int numOfParts;
	private int placedPartCount;
	private bool isGameStarted;

	// Use this for initialization
	void Start () {
		SetCallbackScreen ("Select Gameplay");

//		ShapeObject.LoadShapeTextures ();
		challengeFeedback.OnFinishDisplayingFeedback += HandleOnFinishDisplayingFeedback;
		
		shapeTypeNames = System.Enum.GetNames (typeof(ShapeObject.ShapeType));
		
		shapeModels = new Dictionary<string, GameObject> ();
		for (int i = 0; i < shapeTypeNames.Length; i++) {
			shapeModels.Add(shapeTypeNames[i], Resources.Load<GameObject>("Prefabs/Shape " + shapeTypeNames[i]));
		}
		
		picture = (Transform)Instantiate (Resources.Load<Transform> ("Prefabs/Arranged Pictures/Picture " + selectedPictureName));
		picture.SetParent (canvasWorld, true);
		picture.SetSiblingIndex (0);

		placedPartCount = 0;
		completedPartIndex = new List<int> ();

		GameAnalyticsSDK.GameAnalytics.NewDesignEvent ("play:arrange");
		
		SoundManager.PlaySFXOneShot (clipVOInstruction);

		/* ================================================================================ */
//		pictureParts = new List<Transform> (picture.childCount);
//		picturePartInfoList = new List<PicturePartInfo> (picture.childCount);
//
//		for (int i = 0; i < picture.childCount; i++) {
//			pictureParts.Add(picture.GetChild(i));
//
//			Vector2 partSize = ((RectTransform) picture.GetChild(i)).sizeDelta;
//
//			PicturePartInfo partInfo = new PicturePartInfo();
//			partInfo.position = pictureParts[i].position;
//			partInfo.rotation = pictureParts[i].rotation.eulerAngles;
//			partInfo.scale = pictureParts[i].localScale;
//
//			if (partSize.x > partSize.y) {
//				partInfo.size = partSize.x * partInfo.scale.x * 1.41f;
//			} else {
//				partInfo.size = partSize.y * partInfo.scale.y * 1.41f;
//			}
//
//			/* Processing angle for width and height calculation after rotation */
//			float rotationAngle = partInfo.rotation.z;
//			rotationAngle = Mathf.Floor(rotationAngle);
//			rotationAngle = Mathf.Abs(rotationAngle);
//			if (rotationAngle > 180) {
//				rotationAngle = rotationAngle - 180;
//			}
//
//			if (rotationAngle > 90) {
//				rotationAngle = rotationAngle - 90;
//			}
//
//			rotationAngle = rotationAngle / 180 * Mathf.PI;
//			/* */
//
//			partInfo.width = (Mathf.Sin(rotationAngle) * partSize.y * partInfo.scale.y + Mathf.Cos(rotationAngle) * partSize.x * partInfo.scale.x);// + 0.4f;
//			partInfo.height = (Mathf.Sin(rotationAngle) * partSize.x * partInfo.scale.x + Mathf.Cos(rotationAngle) * partSize.y * partInfo.scale.y);// + 0.4f;
//
//			picturePartInfoList.Add(partInfo);
//		}
		/* ================================================================================ */
//
//		RandomizePartPositions ();
	}

	protected override void OnUpdateCalled () {
		if (Time.timeSinceLevelLoad >= 3.5f && !isGameStarted) {
			blocker.SetActive(false);
			isGameStarted = true;
			
			RectTransform parts = (RectTransform)Instantiate (Resources.Load<RectTransform> ("Prefabs/Arranged Pictures/Parts " + selectedPictureName));
			parts.SetParent (shapeContainer);
			parts.localPosition = new Vector3 ();
			
			for (int i = 0; i < parts.childCount; i++) {
				parts.GetChild(i).GetComponent<DraggableObject>().OnFinishDrag += HandleOnPartsFinishedDrag;
			}
			
			numOfParts = picture.childCount;
		}
	}

	void HandleOnPartsFinishedDrag(GameObject draggedObject) {
		bool isPositionCorrect = false;

		for (int i = 0; i < picture.childCount; i++) {
			if (Mathf.Abs (draggedObject.transform.position.x - picture.GetChild(i).position.x) <= 0.3f 
			    && Mathf.Abs (draggedObject.transform.position.y - picture.GetChild(i).position.y) <= 0.3f) {
				if (draggedObject.transform.rotation == picture.GetChild(i).rotation && draggedObject.transform.localScale == picture.GetChild(i).localScale && !completedPartIndex.Contains(i)) {
					draggedObject.transform.position = picture.GetChild(i).position;
//					draggedObject.transform.SetParent (picture, true);

					draggedObject.GetComponent<ShapeObject>().DisableDrag();
					isPositionCorrect = true;
					++placedPartCount;
					completedPartIndex.Add(i);
					break;
				}
			}
		}

		if (isPositionCorrect) {
			if (draggedObject.GetComponent<ShapeObject>().shapeAnimator != null) {
				draggedObject.GetComponent<ShapeObject>().shapeAnimator.SetTrigger("Correct");
				
				challengeFeedback.DisplayFeedback(ChallengeFeedback.CODE_CORRECT);
			}
		} else {
			draggedObject.GetComponent<DraggableObject>().ReturnToInitialPosition();
			
			if (draggedObject.GetComponent<ShapeObject>().shapeAnimator != null) {
				draggedObject.GetComponent<ShapeObject>().shapeAnimator.SetTrigger("Wrong");
				
				challengeFeedback.DisplayFeedback(ChallengeFeedback.CODE_WRONG);
			}
		}

		if (placedPartCount == numOfParts) {
			// Trigger Gameover
			picture.GetComponent<SpriteRenderer>().enabled = false;
//			Invoke("TriggerGameOver", 1.5f);
		}
	}
	
	void HandleOnFinishDisplayingFeedback () {
		if (placedPartCount == numOfParts) {
			// Trigger GameOver
			blocker.SetActive(true);
			Invoke("TriggerGameover", 0.5f);
		}
	}
	
	void TriggerGameover() {
		RectTransform resultPopup = (RectTransform) Instantiate(Resources.Load<RectTransform>("Prefabs/ResultPopup"));
		resultPopup.SetParent(transform, false);
		resultPopup.GetComponentInChildren<ClickableObject> ().OnClick += HandleOnRestartClicked;
		
		SoundManager.PlaySFXOneShot(SoundManager.clipsVOResult[Random.Range(0, SoundManager.clipsVOResult.Length)]);
	}
	
	void HandleOnRestartClicked(GameObject theObject) {
		Application.LoadLevel ("Pregame Arrange");
	}

	void RandomizePartPositions() {
//		int numOfColumn = 10;
//		int numOfRow = 10;
//		float gridWidth = shapeContainer.sizeDelta.x / numOfColumn;
//		float gridHeight = shapeContainer.sizeDelta.y / numOfRow;
//
//		List<int> rowPositionOfEmptyGrid = new List<int> (numOfColumn);
//		for (int i = 0; i < numOfColumn; i++) {
//			rowPositionOfEmptyGrid.Add(0);
//		}
//
//		for (int i = 0; i < pictureParts.Count; i++) {
//			int shapeWidthInGrid = Mathf.FloorToInt(picturePartInfoList[i].width / gridWidth) + 1;
//			int shapeHeightInGrid = Mathf.FloorToInt(picturePartInfoList[i].height / gridHeight) + 1;
//
//			List<int> availableColumnIndex = new List<int>();
//			for (int j = 0; j < numOfColumn; j++) {
//				availableColumnIndex.Add(j);
//			}
//
//			bool placeFound = false;
//
//			while (!placeFound) {
//				/* Find column index that contains smallest row position with empty grid */
//				int benchmarkValue = 999;
//				int minColumnIndex = -1;
//
//				for (int j = 0; j < availableColumnIndex.Count; j++) {
//					if (rowPositionOfEmptyGrid[availableColumnIndex[j]] < benchmarkValue) {
//						benchmarkValue = rowPositionOfEmptyGrid[availableColumnIndex[j]];
//						minColumnIndex = availableColumnIndex[j];
//					}
//				}
//				/* */
//				
//				Debug.Log("Got column candidate");
//				Debug.Log(minColumnIndex + " " + rowPositionOfEmptyGrid[minColumnIndex]);
//
//				/* Check space availability */
//				if (shapeWidthInGrid <= numOfColumn - minColumnIndex) {
//					bool isExistAnotherShapeOnRight = false;
//
//					for (int j = minColumnIndex + 1; j < minColumnIndex + shapeWidthInGrid; j++) {
//						if (rowPositionOfEmptyGrid[j] > rowPositionOfEmptyGrid[minColumnIndex]) {
//							isExistAnotherShapeOnRight = true;
//						}
//					}
//
//					if (!isExistAnotherShapeOnRight) {
//						placeFound = true;
//						Debug.Log("Found Place");
//						Debug.Log(minColumnIndex + " ___ " + rowPositionOfEmptyGrid[minColumnIndex] + " ___ " + shapeWidthInGrid + " ___ " + shapeHeightInGrid);
//						// Space available, place shape here
//						float shapePositionX = (-shapeContainer.sizeDelta.x / 2) + (gridWidth * minColumnIndex) + (picturePartInfoList[i].width / 2);
//						float shapePositionY = (shapeContainer.sizeDelta.y / 2) - (gridHeight * rowPositionOfEmptyGrid[minColumnIndex]) - (picturePartInfoList[i].height / 2);
//
//						pictureParts[i].localPosition = new Vector3(shapePositionX, shapePositionY);
//
//						// Mark filled grid
//						int curRow = rowPositionOfEmptyGrid[minColumnIndex];
//						for (int j = minColumnIndex; j < minColumnIndex + shapeWidthInGrid; j++) {
//							rowPositionOfEmptyGrid[j] = curRow + shapeHeightInGrid;
//						}
//
//						string debugText = "";
//						for (int j = 0; j < numOfColumn; j++) {
//							debugText += rowPositionOfEmptyGrid[j] + " ";
//						}
//						Debug.Log(debugText);
//					}
//				}
//				/* */
//
//				/* Remove current column index from list, if space not available */
//				if (!placeFound) {
//					availableColumnIndex.Remove(minColumnIndex);
//				}
//				/* */
//			}
//		}
		/*====================================================================================================================================== */
//		int i = 0;
//		
//		List<Transform> shapes = new List<Transform> ();
//
//		while (i < pictureParts.Count) {
//			Transform shape = pictureParts[i];
//			Vector2 shapeDimension = shape.GetComponent<RectTransform>().sizeDelta;
//
//			bool isPositionOk = false;
//			int numOfTry = 0;
//			
//			while (!isPositionOk) {
//				isPositionOk = true;
//
//				shape.localPosition = new Vector3(Random.Range(-shapeContainer.sizeDelta.x / 2 + shapeDimension.x / 2, shapeContainer.sizeDelta.x / 2 - shapeDimension.x / 2), 
//				                                  Random.Range(-shapeContainer.sizeDelta.y / 2 + shapeDimension.y / 2, shapeContainer.sizeDelta.y / 2 - shapeDimension.y / 2));
//				
//				for (int j = 0; j < shapes.Count; j++) {
//					if (shape.GetComponentInChildren<Collider2D>().bounds.Intersects(shapes[j].GetComponentInChildren<Collider2D>().bounds)) {
//						isPositionOk = false;
//					}
//				}
//				
//				++numOfTry;
//				
//				if (!isPositionOk && numOfTry > 50) {
////					for (int j = 0; j < shapes.Count; j++) {
////						shapes[j].localPosition = new Vector3();
////					}
////					
////					shapes.Clear();
////					
////					i = 0;
////					shape = pictureParts[i];
//
//					numOfTry = 0;
//					break;
//				}
//				
//				if (isPositionOk) {
//					numOfTry = 0;
//				}
//			}
//			
//			shapes.Add(shape);
//			++i;
//		}
//	}
//
//	void SortShapesByLength() {
//		ShapeComparer comparer = new ShapeComparer ();
//	}
//
//	class ShapeComparer : IComparer<PicturePartInfo> {
//		public int Compare (PicturePartInfo part1, PicturePartInfo part2) {
//			return part1.width.CompareTo(part2.width);
//		}
	}
}

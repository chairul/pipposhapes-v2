﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using SimpleJSON;

public class DataManager {

	public enum Locale {EN, ID};

	public static Locale locale;
	public static bool isAdsRemoved;
	public static string gameID = "shapes";

	public static void NewData() {
		/* === Init game data === */
		// Set Sound State
		SoundManager.isBgmOn = true;
		SoundManager.isSfxOn = true;
		
		// <Init game-spesific data>
		isAdsRemoved = false;
		/* === === */

		SaveData ();
	}

	public static void LoadData() {
		// Set Locale
		locale = Locale.ID;

		if (File.Exists (Application.persistentDataPath + "/game_data.json")) {
			/* === Read raw string from external file === */
			StreamReader reader = new StreamReader(Application.persistentDataPath + "/game_data.json");
			string saveDataString = reader.ReadToEnd();
			reader.Close();
			/* === === */

			/* === Parse string to json object === */
			var saveData = JSONNode.Parse(saveDataString);
			/* === === */

			/* === Extract game data from json object === */
			SoundManager.isSfxOn = saveData["sfx"].AsBool;
			SoundManager.isBgmOn = saveData["bgm"].AsBool;

			isAdsRemoved = saveData["ads"].AsBool;

			// <Extract game-spesific data>

			/* === === */
		} else {
			FileStream file = File.Create(Application.persistentDataPath + "/game_data.json");
			file.Close();

			NewData();
		}

		OtherGameAdManager.LoadInfoData ();
	}

	public static void SaveData() {
		/* === Create save data structure === */
		var saveData = new JSONClass ();
		/* === === */
		
		/* === Fill structure with game data === */
		saveData ["sfx"].AsBool = SoundManager.isSfxOn;
		saveData ["bgm"].AsBool = SoundManager.isBgmOn;

		saveData ["ads"].AsBool = isAdsRemoved;

		// <Fill with game-spesific data>

		/* === === */
		
		/* === Write data to external file === */
		StreamWriter writer = new StreamWriter (Application.persistentDataPath + "/game_data.json");
		writer.Write (saveData.ToString ());
		writer.Flush ();
		writer.Close ();
		/* === === */
	}
	
	public static string GetStoreAddressLink(string id = "") {
		if (id == "") {
			return "http://play.google.com/store/apps/details?id=com.arsakids." + locale.ToString ().ToLower () + ".pippolearn" + gameID;
		} else {
			return "http://play.google.com/store/apps/details?id=com.arsakids." + locale.ToString ().ToLower () + ".pippolearn" + id;
		}
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameFindManager : SceneManager {

	public BoxCollider2D shapeArea;
	public Transform canvasWorld;
	public Text textCorrectCount;
	public Text textWrongCount;
	public ChallengeFeedback challengeFeedback;
	public GameObject blocker;
	public AudioClip clipVOInstruction;

	private readonly int NUM_OF_CHOICE = 4;

	private int correctAnswerCount;
	private int wrongAnswerCount;
	private int numOfChallenge;
	private bool isGameStarted;
	private string[] shapeTypeNames;
	private string challenge;
	private Dictionary<string, GameObject> shapeModels;

	// Use this for initialization
	void Start () {
		SetCallbackScreen ("Select Gameplay");

//		ShapeObject.LoadShapeTextures ();
		challengeFeedback.OnFinishDisplayingFeedback += HandleOnFinishDisplayingFeedback;

		shapeTypeNames = System.Enum.GetNames (typeof(ShapeObject.ShapeType));

		shapeModels = new Dictionary<string, GameObject> ();
		for (int i = 0; i < shapeTypeNames.Length; i++) {
			shapeModels.Add(shapeTypeNames[i], Resources.Load<GameObject>("Prefabs/Shape " + shapeTypeNames[i]));
		}

		GameAnalyticsSDK.GameAnalytics.NewDesignEvent ("play:find");
		
		SoundManager.PlaySFXOneShot (clipVOInstruction);
	}
	
	// Update is called once per frame
	protected override void OnUpdateCalled () {
		if (Time.timeSinceLevelLoad >= 3.5f && !isGameStarted) {
			isGameStarted = true;
			blocker.SetActive(false);
			
			GenerateChallenge ();
		}
	}
	
	void GenerateChallenge () {
		/* Pick random shape as challenge and dominate */
		challenge = shapeTypeNames [Random.Range (0, shapeTypeNames.Length)];

		string dominatingShape;

		do {
			dominatingShape = shapeTypeNames [Random.Range (0, shapeTypeNames.Length)];
		} while (dominatingShape == challenge);
		/* */

		/* Remove previously generated shape */
		if (canvasWorld.childCount > 0) {
			for (int i = 0; i < canvasWorld.childCount; i++) {
				Destroy(canvasWorld.GetChild(i).gameObject);
			}
		}
		/* */
		
		/* Pick random index for shape position */
		int challengeIndex = Random.Range (0, NUM_OF_CHOICE);
		/* */

		/* Select two texture index */
		List<int> availableTextureIndex = new List<int> ();
		for (int i = 0; i < ShapeObject.shapeTextures.Length; i++) {
			availableTextureIndex.Add(i);
		}

//		int challengeTextureIndex = Random.Range (0, ShapeObject.shapeTextures.Length);
//		int dominatingTextureIndex = Random.Range (0, ShapeObject.shapeTextures.Length);
//
//		while (dominatingTextureIndex == challengeTextureIndex) {
//			dominatingTextureIndex = Random.Range (0, ShapeObject.shapeTextures.Length);
//		}
		/* */

		float spaceWidth = shapeArea.size.x / NUM_OF_CHOICE;
		float spaceHeight = shapeArea.size.y;
		float leftBorder = shapeArea.transform.position.x - (shapeArea.size.x / 2) + (spaceWidth / 2);

		for (int i = 0; i < NUM_OF_CHOICE; i++) {
			GameObject shape;

			if (i == challengeIndex) {
				// Generate shape with challenge type
				shape = (GameObject) Instantiate(shapeModels[challenge]);
//				shape.GetComponent<ShapeObject>().SetTexture(challengeTextureIndex);
			} else {
				// Generate shape with dominating type
				shape = (GameObject) Instantiate(shapeModels[dominatingShape]);
//				shape.GetComponent<ShapeObject>().SetTexture(dominatingTextureIndex);
			}

			int textureRng = availableTextureIndex[Random.Range(0, availableTextureIndex.Count)];
			availableTextureIndex.Remove(textureRng);
			shape.GetComponent<ShapeObject>().SetTexture(textureRng);

			float shapeWidth = shape.GetComponent<RectTransform>().sizeDelta.x;
			float shapeHeight = shape.GetComponent<RectTransform>().sizeDelta.y;
			float shapePositionY = Random.Range(shapeArea.transform.position.y - (spaceHeight / 2) + (shapeHeight / 2), shapeArea.transform.position.y + (spaceHeight/2) - (shapeHeight / 2));

			shape.transform.SetParent(canvasWorld);
			shape.transform.localPosition = new Vector3(leftBorder + (i * spaceWidth), shapePositionY);

			float scale = Random.Range(0.4f, 1f);
			shape.transform.localScale = new Vector3(scale, scale);

			shape.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, Random.Range(0f, 180f)));

			shape.GetComponent<ShapeObject>().OnClick += HandleOnShapeClicked;
			shape.GetComponent<ShapeObject>().DisableDrag();
		}
	}

	void HandleOnShapeClicked(ShapeObject shape) {
		shape.GetComponentInChildren<Animator> ().SetTrigger ("Touch");

		if (shape.type.ToString () == challenge) {
			// Correct Answer
			++correctAnswerCount;
			textCorrectCount.text = correctAnswerCount.ToString();
			
			challengeFeedback.DisplayFeedback(ChallengeFeedback.CODE_CORRECT);
		} else {
			// Wrong Answer
			++wrongAnswerCount;
			textWrongCount.text = wrongAnswerCount.ToString();
			
			challengeFeedback.DisplayFeedback(ChallengeFeedback.CODE_WRONG);
		}
	}
	
	void HandleOnFinishDisplayingFeedback () {
		++numOfChallenge;
		
		if (numOfChallenge == 10) {
			blocker.SetActive(true);
			Invoke("TriggerGameover", 0.5f);
		} else {
			GenerateChallenge ();
		}
	}

	void TriggerGameover() {
		RectTransform resultPopup = (RectTransform) Instantiate(Resources.Load<RectTransform>("Prefabs/ResultPopup"));
		resultPopup.SetParent(transform, false);
		resultPopup.GetComponentInChildren<ClickableObject> ().OnClick += HandleOnRestartClicked;
		
		SoundManager.PlaySFXOneShot(SoundManager.clipsVOResult[Random.Range(0, SoundManager.clipsVOResult.Length)]);
	}

	void HandleOnRestartClicked(GameObject theObject) {
		Application.LoadLevel (Application.loadedLevelName);
	}
}

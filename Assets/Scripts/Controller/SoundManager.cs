﻿using UnityEngine;
using System.Collections;

public class SoundManager {
	
	public static bool isSfxOn;
	public static bool isBgmOn;
	public static AudioSource bgm;
	public static AudioClip clipVOSelectGameplay;
	public static AudioClip clipSFXGeneralButton;
	public static AudioClip clipSFXHomeButton;
	public static AudioClip clipSFXFeedbackCorrect;
	public static AudioClip clipSFXFeedbackCorrect2;
	public static AudioClip clipSFXFeedbackWrong;
	public static AudioClip clipSFXShapeClicked;
	public static AudioClip[] clipsVOPositiveFeedback;
	public static AudioClip[] clipsVONegativeFeedback;
	public static AudioClip[] clipsVOResult;
	
	private static GameObject prefabSfxOneShot;
	private static GameObject vo;
	
	public static void Initialize() {
		/* === Create AudioSource object for playing BGM === */
		GameObject bgmObject = (GameObject)MonoBehaviour.Instantiate (Resources.Load<GameObject>("Prefabs/BGMObject"));
		bgm = bgmObject.GetComponent<AudioSource> ();
		bgm.mute = !isBgmOn;
		MonoBehaviour.DontDestroyOnLoad (bgm.gameObject);
		/* === === */
		
		/* === Load SFX One Shot Prefab === */
		prefabSfxOneShot = Resources.Load<GameObject> ("Prefabs/SFXOneShot");
		/* === === */
		
		/* === Load Audio Clip === */
		clipVOSelectGameplay = Resources.Load<AudioClip> ("VO/vo_select_gameplay");
		clipSFXGeneralButton = Resources.Load<AudioClip> ("SFX/sfx_general_button");
		clipSFXHomeButton = Resources.Load<AudioClip> ("SFX/sfx_home_button");
		clipSFXFeedbackCorrect = Resources.Load<AudioClip> ("SFX/sfx_feedback_correct");
		clipSFXFeedbackCorrect2 = Resources.Load<AudioClip> ("SFX/sfx_feedback_correct_2");
		clipSFXFeedbackWrong = Resources.Load<AudioClip> ("SFX/sfx_feedback_wrong");
		clipSFXShapeClicked = Resources.Load<AudioClip> ("SFX/sfx_shape_clicked");
		clipsVOPositiveFeedback = Resources.LoadAll<AudioClip> ("VO/Feedback Positive");
		clipsVONegativeFeedback = Resources.LoadAll<AudioClip> ("VO/Feedback Negative");
		clipsVOResult = Resources.LoadAll<AudioClip> ("VO/Result");
		/* === === */
	}
	
	public static void PlayBGM(AudioClip bgmClip) {
		if (bgm.clip == null || bgm.clip.name != bgmClip.name) {
			bgm.clip = bgmClip;
			bgm.volume = 0.4f;
			bgm.Play ();
		}
	}
	
	public static void PlaySFXOneShot(AudioClip sfxClip) {
		if (isSfxOn) {
			GameObject sfxOneShot = (GameObject)MonoBehaviour.Instantiate (prefabSfxOneShot);
			
			if (sfxClip.name.Contains("vo_")) {
				if (vo != null) {
					GameObject.Destroy(vo);
				}
				
				vo = sfxOneShot;

				sfxOneShot.GetComponent<AudioSource>().volume = 0.5f;
			} else {
				sfxOneShot.GetComponent<AudioSource>().volume = 0.9f;
			}
			
			sfxOneShot.GetComponent<SFXOneShot> ().Play (sfxClip);
		}
	}
}

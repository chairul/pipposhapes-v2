﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameMatchManager : SceneManager {

	private readonly float MATCHING_DISTANCE = 0.75f;
	private readonly int NUM_OF_SHAPES = 10;
	private readonly int[] shapeTextureIndex = {1, 3, 0, 5, 2, 4};

	public RectTransform shapeContainer;
	public Transform shapeClassA;
	public Transform shapeClassB;
	public Transform shapeClassC;
	public Text textCorrectCount;
	public Text textWrongCount;
	public ChallengeFeedback challengeFeedback;
	public GameObject blocker;
	public AudioClip clipVOInstruction;
	
	private string[] shapeTypeNames;
	private Dictionary<string, GameObject> shapeModels;
	private List<string> chosenShapeTypeNames;
	private List<int> chosenShapeTextureIndex;
	private int scoreCorrectCount;
	private int scoreWrongCount;
	private int numOfRemainingShapes;
	private bool isGameStarted;

	[HideInInspector]
	public bool isShapeMatched;
	private bool isShapeMeetTarget;
	private int numOfTargetMeet;

	// Use this for initialization
	void Start () {
		SetCallbackScreen ("Select Gameplay");

//		ShapeObject.LoadShapeTextures ();
		challengeFeedback.OnFinishDisplayingFeedback += HandleOnFinishDisplayingFeedback;
		
		shapeTypeNames = System.Enum.GetNames (typeof(ShapeObject.ShapeType));
		
		shapeModels = new Dictionary<string, GameObject> ();
		for (int i = 0; i < shapeTypeNames.Length; i++) {
			shapeModels.Add(shapeTypeNames[i], Resources.Load<GameObject>("Prefabs/Shape " + shapeTypeNames[i]));
		}

		/* Pick three random shapes */
		chosenShapeTypeNames = new List<string> ();
		chosenShapeTextureIndex = new List<int> ();

		string randomShapeTypeName;

		for (int i = 0; i < 3; i++) {
			int rng;

			do {
				rng = Random.Range (0, shapeTypeNames.Length);
				randomShapeTypeName = shapeTypeNames [rng];
			} while (chosenShapeTypeNames.Contains(randomShapeTypeName));

			chosenShapeTypeNames.Add(randomShapeTypeName);
			chosenShapeTextureIndex.Add(shapeTextureIndex[rng]);
		}

		/* Assign shape to each class */
		GameObject chosenShape = (GameObject)Instantiate (Resources.Load<GameObject>("Prefabs/Shape " + chosenShapeTypeNames [0] + " NP"));
		chosenShape.transform.SetParent (shapeClassA);
		chosenShape.transform.localPosition = new Vector3 ();
		chosenShape.GetComponentInChildren<ShapeObject> ().DisableDrag ();
		
		chosenShape = (GameObject)Instantiate (Resources.Load<GameObject>("Prefabs/Shape " + chosenShapeTypeNames [1] + " NP"));
		chosenShape.transform.SetParent (shapeClassB);
		chosenShape.transform.localPosition = new Vector3 ();
		chosenShape.GetComponentInChildren<ShapeObject> ().DisableDrag ();
		
		chosenShape = (GameObject)Instantiate (Resources.Load<GameObject>("Prefabs/Shape " + chosenShapeTypeNames [2] + " NP"));
		chosenShape.transform.SetParent (shapeClassC);
		chosenShape.transform.localPosition = new Vector3 ();
		chosenShape.GetComponentInChildren<ShapeObject> ().DisableDrag ();

		numOfTargetMeet = 0;

		GameAnalyticsSDK.GameAnalytics.NewDesignEvent ("play:match");
		
		SoundManager.PlaySFXOneShot (clipVOInstruction);
	}
	
	// Update is called once per frame
	protected override void OnUpdateCalled () {
		if (Time.timeSinceLevelLoad >= 3.5f && !isGameStarted) {
			isGameStarted = true;
			blocker.SetActive(false);
			
			GenerateAndFillShapes ();
		}
	}

	void GenerateAndFillShapes() {
		int i = 0;
		
		int[] shapesTypeCount = new int[chosenShapeTypeNames.Count];
		for (int j = 0; j < shapesTypeCount.Length; j++) {
			shapesTypeCount[j] = 0;
		}

		List<GameObject> shapes = new List<GameObject> ();
		List<GameObject> shapeCandidates = new List<GameObject> ();

		List<int> availableTextureIndex = new List<int> ();
		for (int idx = 0; idx < ShapeObject.shapeTextures.Length; idx++) {
			availableTextureIndex.Add(idx);
		}

		for (int j = 0; j < NUM_OF_SHAPES; j++) {
			int rng = Random.Range(0, chosenShapeTypeNames.Count);

			if (j < 9) {
				while (shapesTypeCount[rng] >= 3) {
					rng = Random.Range(0, chosenShapeTypeNames.Count);
				}
			}

			++shapesTypeCount[rng];

			GameObject shape = (GameObject) Instantiate(shapeModels[chosenShapeTypeNames[rng]]);
			shape.transform.SetParent(shapeContainer);

			float randomScale = Random.Range(0.3f, 0.5f);
			shape.transform.localScale = new Vector3(randomScale, randomScale);
			shape.GetComponent<DraggableObject>().OnFinishDrag += HandleOnShapeFinishedDrag;
			shape.GetComponentInChildren<AnimationEvent>().OnAnimationFinished += HandleShapeAnimationFinished;

			if (availableTextureIndex.Count == 0) {
				for (int idx = 0; idx < ShapeObject.shapeTextures.Length; idx++) {
					availableTextureIndex.Add(idx);
				}
			}

			int textureRng = availableTextureIndex[Random.Range(0, availableTextureIndex.Count)];
			availableTextureIndex.Remove(textureRng);

			shape.GetComponent<ShapeObject>().SetTexture(textureRng);

			shapeCandidates.Add(shape);
		}

		while (i < NUM_OF_SHAPES) {
			GameObject shape = shapeCandidates[i];
			Vector2 shapeDimension = shape.GetComponent<RectTransform>().sizeDelta;

			bool isPositionOk = false;
			int numOfTry = 0;

			while (!isPositionOk) {
				isPositionOk = true;
				
				shape.transform.localPosition = new Vector3(Random.Range(-shapeContainer.sizeDelta.x / 2 + shapeDimension.x / 2, shapeContainer.sizeDelta.x / 2 - shapeDimension.x / 2), 
				                                  Random.Range(-shapeContainer.sizeDelta.y / 2 + shapeDimension.y / 2, shapeContainer.sizeDelta.y / 2 - shapeDimension.y / 2));

				for (int j = 0; j < shapes.Count; j++) {
					if (shape.GetComponentInChildren<Collider2D>().bounds.Intersects(shapes[j].GetComponentInChildren<Collider2D>().bounds)) {
						isPositionOk = false;
					}
				}

				++numOfTry;

				if (!isPositionOk && numOfTry > 30) {
//					for (int j = 0; j < shapes.Count; j++) {
//						shapes[j].transform.localPosition = new Vector3();
//					}
//
//					shapes.Clear();
//					
//					i = 0;
//					shape = shapeCandidates[i];

					numOfTry = 0;
					break;
				}

				if (isPositionOk) {
					numOfTry = 0;
				}
			}

//			shape.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, Random.Range(0, 180f)));
			shapes.Add(shape);
			++i;
		}

		numOfRemainingShapes = NUM_OF_SHAPES;
	}

	void HandleShapeAnimationFinished (GameObject aObject) {
		Destroy (aObject.transform.parent.gameObject);
	}

	void HandleOnShapeFinishedDrag(GameObject draggedObject) {
		if (isShapeMatched) {
//			Destroy (draggedObject);
			draggedObject.GetComponent<ShapeObject>().DisableDrag();
			draggedObject.GetComponentInChildren<Animator>().SetTrigger("Correct");

			// Process Correct Answer
			++scoreCorrectCount;
			textCorrectCount.text = scoreCorrectCount.ToString();
			
			challengeFeedback.DisplayFeedback(ChallengeFeedback.CODE_CORRECT);
			--numOfRemainingShapes;
		} else {
			draggedObject.GetComponent<DraggableObject>().ReturnToInitialPosition ();
			draggedObject.GetComponentInChildren<Animator>().SetTrigger("Wrong");

			if (isShapeMeetTarget) {
				// Process Wrong Answer
				++scoreWrongCount;
				textWrongCount.text = scoreWrongCount.ToString();
				
				challengeFeedback.DisplayFeedback(ChallengeFeedback.CODE_WRONG);
			}
		}

		isShapeMatched = false;
		isShapeMeetTarget = false;
		numOfTargetMeet = 0;
	}

	public void MeetTarget() {
		++numOfTargetMeet;
		isShapeMeetTarget = true;
	}

	public void LeaveTarget() {
		--numOfTargetMeet;

		if (numOfTargetMeet == 0) {
			isShapeMeetTarget = false;
		}
	}
	
	void HandleOnFinishDisplayingFeedback () {
		if (numOfRemainingShapes == 0) {
			// Trigger GameOver
			blocker.SetActive(true);
			Invoke("TriggerGameover", 0.5f);
		}
	}
	
	void TriggerGameover() {
		RectTransform resultPopup = (RectTransform) Instantiate(Resources.Load<RectTransform>("Prefabs/ResultPopup"));
		resultPopup.SetParent(transform, false);
		resultPopup.GetComponentInChildren<ClickableObject> ().OnClick += HandleOnRestartClicked;
		
		SoundManager.PlaySFXOneShot(SoundManager.clipsVOResult[Random.Range(0, SoundManager.clipsVOResult.Length)]);
	}
	
	void HandleOnRestartClicked(GameObject theObject) {
		Application.LoadLevel (Application.loadedLevelName);
	}
}

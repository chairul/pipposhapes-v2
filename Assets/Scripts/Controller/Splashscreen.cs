﻿using UnityEngine;
using System.Collections;

public class Splashscreen : SceneManager {
	
	private readonly float SPLASH_TIME = 3.5f;
	
	private bool isVOPlayed;
	
	public AudioClip clipSfxSplashscreen;
	
	/*
	 * Initialization
	 */
	void Start() {
		/* === Load game data === */
		AchievementManager.LoadData ();
		DataManager.LoadData ();
		LocalizedContent.LoadText ();
		/* === === */
		
		/* === Initialize General Component === */
		SoundManager.Initialize ();
		MessageManager.Initialize ();
		AdsManager.Init ();
		InAppManager.Initialize ();
		/* === === */
		
		/* === Initialize Game Spesific Component === */
		ShapeObject.LoadShapeTextures ();
		/* === === */
		
		SoundManager.PlaySFXOneShot (clipSfxSplashscreen);
	}
	
	/*
	 * Customized update call
	 */
	protected override void OnUpdateCalled () {
		if (!isVOPlayed && Time.timeSinceLevelLoad >= 0.5f) {
			/* === Play Splashscreen VO === */
			SoundManager.PlaySFXOneShot (Resources.Load<AudioClip> ("SFX/vo_splashscreen"));
			isVOPlayed = true;
			/* === === */
		}
		
		if (Time.timeSinceLevelLoad >= SPLASH_TIME) {
			SoundManager.PlayBGM(Resources.Load<AudioClip>("BGM/bgm_menu"));
			Application.LoadLevel("Mainmenu");
		}
	}
}

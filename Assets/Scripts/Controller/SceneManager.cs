﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour {

	protected readonly string QUIT = "Quit";

	private string callbackScreen;				// Screen name that will be called on Back Button Event
	private GameObject promptWindowModel;		// Prefab Model for prompt window on Back Button Event
	private bool isPromptWindowActive;			// Flag to indicate if prompt window is already active
	
	/*
	 * Base Update Function
	 */
	void Update () {
		// Back button event
		if (Input.GetKeyUp (KeyCode.Escape)) {
			if (promptWindowModel != null && !isPromptWindowActive) {
				// <Instantiate prompt window>
				
				// <Set navigate function as listener on prompt window>
				// <Set canceled listener to prompt window>

				isPromptWindowActive = true;
			} else {
				NavigateToCallbackScreen();
			}
		}

		// Custom update handler
		OnUpdateCalled ();
	}

	/*
	 * Handler for customized Update() implementation on child class
	 */
	protected virtual void OnUpdateCalled() {

	}

	/*
	 * Function to set the callback screen for Back Button Event
	 */
	protected void SetCallbackScreen(string screenName, GameObject promptModel = null) {
		callbackScreen = screenName;
		promptWindowModel = promptModel;
	}

	/*
	 * Function to navigate to registered callback screen
	 */
	void NavigateToCallbackScreen() {
		if (callbackScreen == QUIT) {
			// Save Data
			DataManager.SaveData ();

			// Quit Application
			Application.Quit ();
		} else {
			Application.LoadLevel(callbackScreen);
		}
	}

	/*
	 * Handler for canceled prompt window
	 */
	void OnPromptWindowCanceled() {
		isPromptWindowActive = false;
	}
}

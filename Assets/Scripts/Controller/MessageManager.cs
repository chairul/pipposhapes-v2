﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MessageManager : MonoBehaviour {

	private static GameObject messagePopupModel;

	public static void Initialize() {
		messagePopupModel = Resources.Load<GameObject> ("Prefabs/Canvas Message");
	}

	public static void DisplayMessage(string content) {
		GameObject messagePopup = (GameObject)Instantiate (messagePopupModel);
		messagePopup.GetComponentInChildren<MessageManager> ().Init (content);
	}

	public Text textContent;

	public void Init(string content) {
		textContent.text = content;
	}

	public void CloseWindow() {
		Destroy (transform.parent.gameObject);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundQAnswer : MonoBehaviour {
	public int answer;
	public SoundQPicker picker;

	public void Click () {
		if (answer == picker.currentQuiz) {
			//indicator
			picker.feedback.DisplayFeedback("correct");
			picker.rightAnswered++;
			picker.rightScore.text = picker.rightAnswered.ToString ();
			picker.answerCode = 1;
		} else {
			//indicator
			picker.feedback.DisplayFeedback("wrong");
			picker.wrongAnswered++;
			picker.worngScore.text = picker.wrongAnswered.ToString ();
			picker.answerCode = 0;
		}
		picker.isAnswered = true;
	}
}

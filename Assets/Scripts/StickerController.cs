﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StickerController : MonoBehaviour {
	public GameObject[] levels;
	public List<Stickable> stickerables = new List<Stickable>();
	public List<GameObject> stickers = new List<GameObject>();
	public GameObject stickerLayout;
	public GameObject stickerSample;
	const float layoutHeight = 250;
	const float stickerSize = 250;
	const float stickerSpacing = 40;

	public List<GameObject> stickerObjects = new List<GameObject> ();
	public GameObject stickerObjectSample;

	public bool isDraggingShape = false;
	public float dragDelay = 0.1f;
	int draggedShapeIndex = 0;

	//
	public int stickersLeft;
	public bool isAnswered = false;
	float delay = 2f;

	//indicator
	public ChallengeFeedback feedback;
	public GameObject indicator;


	void Start () {
		GetLevels ();
		CreateLevel (Random.Range (0, levels.Length));
		SetStickers ();
	}
		
	void GetLevels () {
		levels = Resources.LoadAll<GameObject> ("Prefabs/ShapeStickerLevels");
	}

	void CreateLevel (int level) {
		GameObject newLevel = Instantiate (levels [level]);
	}

	void SetStickers () {
		for (int i = 0; i < stickerables.Count; i++) {
			GameObject newSticker = Instantiate (stickerSample, stickerLayout.transform);
			newSticker.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("StickerSample/" + stickerables [i].shapeID);
			newSticker.name = i.ToString ();
			newSticker.SetActive (true);
			stickers.Add (newSticker);

			GameObject newStickerObject = Instantiate (stickerObjectSample, this.transform);
			newStickerObject.GetComponent<SpriteRenderer> ().sprite = Resources.Load<Sprite> ("StickerSample/" + stickerables [i].shapeID);
			newStickerObject.GetComponent<Sticker> ().stickerID = stickerables [i].shapeID;
			newStickerObject.GetComponent<Sticker> ().controller = this;
			newStickerObject.GetComponent<Sticker> ().isDragable = true;
			newStickerObject.AddComponent<PolygonCollider2D> ();
			stickerObjects.Add (newStickerObject);
		}
		stickerLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 ((stickers.Count * stickerSize) + (stickers.Count * stickerSpacing), layoutHeight);
		stickerLayout.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (stickerLayout.GetComponent<RectTransform> ().sizeDelta.x, 0);
	}

	void Update () {
		if (isAnswered) {
			for (int i = 0; i < stickers.Count; i++) {
				stickerObjects [i].GetComponent<Sticker> ().isDragable = false;
			}
			delay -= Time.deltaTime;
			if (delay <= 0) {
				for (int i = 0; i < stickers.Count; i++) {
					stickerObjects [i].GetComponent<Sticker> ().isDragable = true;
				}
				delay = 2;
				isAnswered = false;
			}
		}


		if (!isAnswered) {
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
			if (hit.collider != null) {
				if (hit.collider.gameObject.tag == "Sticker") {
					if (Input.GetMouseButton (0)) {
						dragDelay -= Time.deltaTime;
						if (dragDelay <= 0) {
							draggedShapeIndex = int.Parse (hit.collider.gameObject.name);
							stickerLayout.gameObject.transform.parent.gameObject.GetComponent<ScrollRect> ().enabled = false;
							stickers [draggedShapeIndex].GetComponent<Image> ().enabled = false;
							isDraggingShape = true;
							dragDelay = 0.1f;
						}
					}
					if (Input.GetMouseButtonUp (0)) {
						dragDelay = 0.1f;
					}
				}
			}
			if (isDraggingShape) {
				if (Input.GetMouseButton (0)) {
					Vector3 point = Camera.main.ScreenToWorldPoint (Input.mousePosition);
					point.z = -1;
					stickerObjects [draggedShapeIndex].SetActive (true);
					stickerObjects [draggedShapeIndex].transform.position = point;
					Cursor.visible = false;
				}
				if (Input.GetMouseButtonUp (0)) {
					Cursor.visible = true;
					isDraggingShape = false;
					if (stickerObjects [draggedShapeIndex].GetComponent<Sticker> ().isMatch) {
						Destroy (stickers [draggedShapeIndex]);
						stickerObjects [draggedShapeIndex].GetComponent<Sticker> ().transform.position = stickerObjects [draggedShapeIndex].GetComponent<Sticker> ().targetPosition;
						stickerObjects [draggedShapeIndex].GetComponent<Sticker> ().transform.localRotation = stickerObjects [draggedShapeIndex].GetComponent<Sticker> ().targetRotation;
						stickerObjects [draggedShapeIndex].GetComponent<Sticker> ().targetObject.GetComponent<PolygonCollider2D> ().enabled = false;
						stickerLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 (((stickersLeft) * stickerSize) + ((stickersLeft) * stickerSpacing), layoutHeight);
						isAnswered = true;
						stickersLeft--;
						feedback.DisplayFeedback ("correct");
					} else {
						stickerLayout.gameObject.transform.parent.gameObject.GetComponent<ScrollRect> ().enabled = true;
						stickers [draggedShapeIndex].GetComponent<Image> ().enabled = true;
						stickerObjects [draggedShapeIndex].SetActive (false);
						isAnswered = true;
						feedback.DisplayFeedback ("wrong");
					}
				}
			}
		}
		if (stickersLeft <= 0) {
			indicator.SetActive (true);
		}
	}
}

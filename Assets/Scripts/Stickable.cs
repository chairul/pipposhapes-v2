﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stickable : MonoBehaviour {
	public StickerController controller;
	public int shapeID;

	void Awake () {
		controller = GameObject.FindGameObjectWithTag ("GameController").GetComponent<StickerController> ();
		shapeID = int.Parse (this.gameObject.name);
		controller.stickerables.Add (this);
		controller.stickersLeft++;
	}
}

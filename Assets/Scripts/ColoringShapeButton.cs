﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoringShapeButton : MonoBehaviour {
	public ColoringDrawingController controller;
	public int shapeCode;
	public GameObject canvas;
	GameObject newShape;
	//drag
	bool isDragged = false;
	bool beginDrag = false;
	float dragDelay = 0.1f;

	void CreateNewShape () {
		newShape = Instantiate (controller.shapesSummon [shapeCode], controller.shapesPanel.transform.parent.transform);
		newShape.name = controller.shapesSummon [shapeCode].name;
		newShape.GetComponent<SpriteRenderer> ().sortingOrder = controller.currentLayer;
		controller.currentLayer++;
		controller.shapeOnCanvas.Add (newShape);
		newShape.GetComponent<ColoringShape> ().controller = controller;
	}


	void Update () {
		if (beginDrag) {
			dragDelay -= Time.deltaTime;
			if (dragDelay <= 0) {
				isDragged = true;
				CreateNewShape ();
				Vector3 point = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				point.z = -1;
				newShape.transform.position = point;
				newShape.GetComponent<SpriteRenderer> ().sortingLayerName = "CanvasUI";
				dragDelay = 0.1f;
				beginDrag = false;
			}
		}
	}

	void OnMouseDown () {
		isDragged = false;
		beginDrag = true;	
	}

	void OnMouseDrag () {
		if (isDragged) {
			Vector3 point = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			point.z = -1;
			newShape.transform.position = point;
			Cursor.visible = false;
		}
	}

	void OnMouseUp () {
		beginDrag = false;
		dragDelay = 0.1f;
		if (!isDragged) {
			CreateNewShape ();
		}
		else { 
			newShape.GetComponent<SpriteRenderer> ().sortingLayerName = "Default";
			if (!newShape.GetComponent<ColoringShape> ().isOnCanvas || newShape.GetComponent<ColoringShape>().isOutOfBound) {
				Destroy (newShape);
			}
		}
		Cursor.visible = true;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HO_Box : MonoBehaviour {
	public HO_Controller controller;
	[Header("Box Attributes")]
	//Box Type
	public int boxCode;
	//Box Volume
	public float boxFull = 4;
	//Posible Matches
	public int matchShapesCount;
	//This Match
	int matchCode = 0;


	[Header("Shapes")]
	public GameObject[] shapePrefabs;
	public List<GameObject> shapeObjects = new List<GameObject> ();


	[Header("Dragging Props")]
	public float dragDelay = 0.1f;
	public bool isDraggingAShape = false;
	int draggedShapeIndex = 0;
	public int shapeDraggedCount = 0;

	void Start () {
		GenerateShapes ();
	}

	void GenerateShapes () {
		//Random Match
		matchCode = Random.Range (0, matchShapesCount);
		//Get Shape Matches
		shapePrefabs = Resources.LoadAll<GameObject> ("Prefabs/HO_Shapes/" + boxCode.ToString () + "/Match" + matchCode.ToString ());

		//Create Layout and Shapes
		for (int i = 0; i < shapePrefabs.Length; i++) {
			GameObject newPlace = Instantiate (controller.placeSample, controller.placeLayout.transform);
			newPlace.GetComponent<Image> ().sprite = shapePrefabs [i].GetComponent<SpriteRenderer> ().sprite;
			newPlace.GetComponent<RectTransform> ().localRotation = shapePrefabs [i].transform.localRotation;
			newPlace.name = i.ToString ();
			newPlace.SetActive (true);
			controller.shapePlaces.Add (newPlace);

			GameObject newShape = Instantiate (shapePrefabs [i]);
			newShape.GetComponent<HO_Object> ().controller = this;
			newShape.GetComponent<HO_Object> ().objectIndex = i;
			newShape.name = i.ToString ();
			newShape.SetActive (false);
			shapeObjects.Add (newShape);
		}
		controller.placeLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 (controller.placeLayoutWidth, (controller.shapePlaces.Count * controller.shapeSize) + (controller.shapePlaces.Count * controller.shapeSpacing));
		controller.placeLayout.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, -controller.placeLayout.GetComponent<RectTransform> ().sizeDelta.y);
	}

	public void AddObjectToBox (float objectFill) {
		boxFull -= objectFill;
	}
	public void RemoveObjectFromBox (float objectFill) {
		boxFull += objectFill;
	}

	void Update () {
		if (boxFull <= 0) {
			controller.indicator.SetActive (true);
			this.transform.GetChild (0).transform.position = this.transform.position;
			this.transform.GetChild (0).transform.localRotation = this.transform.localRotation;
		}
		if (!isDraggingAShape) {
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
			if (hit.collider != null) {
				if (hit.collider.gameObject.tag == "Shapes" && hit.collider.gameObject.layer == 5) {
					if (Input.GetMouseButton (0)) {
						dragDelay -= Time.deltaTime;
						if (dragDelay <= 0) {
							draggedShapeIndex = int.Parse (hit.collider.gameObject.name);
							controller.placeLayout.gameObject.transform.parent.gameObject.GetComponent<ScrollRect> ().enabled = false;
							controller.shapePlaces [draggedShapeIndex].GetComponent<Image> ().enabled = false;
							isDraggingAShape = true;
							dragDelay = 0.1f;
						}
					}
					if (Input.GetMouseButtonUp (0)) {
						dragDelay = 0.1f;
					}
				}
			}
		} else {
			if (!shapeObjects [draggedShapeIndex].GetComponent<HO_Object> ().isInPlace) {
				if (Input.GetMouseButton (0)) {
					Vector3 point = Camera.main.ScreenToWorldPoint (Input.mousePosition);
					point.z = -1;
					shapeObjects [draggedShapeIndex].SetActive (true);
					shapeObjects [draggedShapeIndex].GetComponent<HO_Object> ().isDragable = true;
					shapeObjects [draggedShapeIndex].transform.position = point;
					Cursor.visible = false;
				}
				if (Input.GetMouseButtonUp (0)) {
					Cursor.visible = true;
					isDraggingAShape = false;
					controller.placeLayout.gameObject.transform.parent.gameObject.GetComponent<ScrollRect> ().enabled = true;
					shapeObjects [draggedShapeIndex].GetComponent<HO_Object> ().isDragable = false;
					if (shapeObjects [draggedShapeIndex].GetComponent<HO_Object> ().isInBox && !shapeObjects [draggedShapeIndex].GetComponent<HO_Object> ().isOnBoundaries && !shapeObjects [draggedShapeIndex].GetComponent<HO_Object> ().isOnOtherShape && !shapeObjects [draggedShapeIndex].GetComponent<HO_Object> ().isInPlace) {
						AddObjectToBox (shapeObjects [draggedShapeIndex].GetComponent<HO_Object> ().objectFill);
						shapeObjects [draggedShapeIndex].GetComponent<HO_Object> ().isInPlace = true;
						controller.shapePlaces [draggedShapeIndex].SetActive (false);
						shapeDraggedCount++;
						controller.placeLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 (controller.placeLayoutWidth, ((controller.shapePlaces.Count - shapeDraggedCount) * controller.shapeSize) + ((controller.shapePlaces.Count - shapeDraggedCount) * controller.shapeSpacing));
						if (boxFull > 0) {
							controller.feedback.DisplayFeedback ("correct");
						}
					} else {
						controller.shapePlaces [draggedShapeIndex].GetComponent<Image> ().enabled = true;
						shapeObjects [draggedShapeIndex].SetActive (false);
						controller.feedback.DisplayFeedback ("wrong");
					}
				}
			}
		}
	}
}

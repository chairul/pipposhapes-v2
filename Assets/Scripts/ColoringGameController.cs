﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoringGameController : MonoBehaviour {
	[Header("Screens")]
	public GameObject mainScreen;
	public GameObject backgroundSelectScreen;
	public GameObject drawingScreen;
	public GameObject galleryScreen;
	public SpriteRenderer background;
	public Sprite[] backgrounds;

	[Header("Background Select")]
	public Sprite selectedBackgroud;

	public GameObject cameraShot;
	public RenderTexture shot;

	public GameObject[] lines;

	void Start () {
		background.sprite = backgrounds [0];
		mainScreen.SetActive (true);
		backgroundSelectScreen.SetActive (false);
		drawingScreen.SetActive (false);
		galleryScreen.SetActive (false);
	}


	public void CreateNewColoring () {
		mainScreen.SetActive (false);
		backgroundSelectScreen.SetActive (true);
	}

	public void OK () {
		background.sprite = backgrounds [1];
		mainScreen.SetActive (false);
		galleryScreen.SetActive (true);
	}

	public void BackToMainScreen () {
		mainScreen.SetActive (true);
		backgroundSelectScreen.SetActive (false);
	}
}

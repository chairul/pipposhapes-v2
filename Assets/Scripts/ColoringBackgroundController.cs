﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColoringBackgroundController : MonoBehaviour {
	public ColoringGameController gameController;
	public Image[] backgroundPreview;
	public Sprite[] backgroundList;
	public int backgroundIndex = 0;

	void Start () {
		gameController = this.GetComponent<ColoringGameController> ();
		backgroundIndex = 1;
		SetBackgroundPreview (backgroundIndex);
	}

	void SetBackgroundPreview (int index) {
		backgroundPreview [0].sprite = backgroundList [Modulo (index - 1, backgroundList.Length)];
		backgroundPreview [1].sprite = backgroundList [index];
		backgroundPreview [2].sprite = backgroundList [Modulo (index + 1, backgroundList.Length)];
	}

	public void NextBackground () {
		backgroundIndex = Modulo (backgroundIndex + 1, backgroundList.Length);
		SetBackgroundPreview (backgroundIndex);
	}

	public void PrevBackground () {
		backgroundIndex = Modulo (backgroundIndex - 1, backgroundList.Length);
		SetBackgroundPreview (backgroundIndex);
	}

	public void SelectBackground () {
		if (gameController.lines != null && gameController.lines.Length > 0) {
			for (int i = 0; i < gameController.lines.Length; i++) {
				gameController.lines [i].SetActive (true);
			}
		}
		gameController.cameraShot.SetActive (true);
		gameController.selectedBackgroud = backgroundList [backgroundIndex];
		this.GetComponent<ColoringDrawingController> ().background.sprite = gameController.selectedBackgroud;
		gameController.drawingScreen.SetActive (true);
		gameController.backgroundSelectScreen.SetActive (false);
	}

	int Modulo (int a, int b) {
		return (a % b + b) % b;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointClickPanel : MonoBehaviour {
	public PointClickController controller;

	void Start () {
		this.GetComponent<BoxCollider2D> ().size = new Vector2 (this.GetComponent<RectTransform> ().rect.width, this.GetComponent<RectTransform> ().rect.height);
	}
}

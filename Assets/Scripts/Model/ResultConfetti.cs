﻿using UnityEngine;
using System.Collections;

public class ResultConfetti : MonoBehaviour {

	public GameObject confettiModel;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CreateConfetti (int indexPosition) {
		GameObject newConfetti = (GameObject)Instantiate (confettiModel);
		newConfetti.GetComponent<Transform> ().SetParent (transform);
		newConfetti.transform.localScale = new Vector3 (1f, 1f);

		if (indexPosition == 1) {
			newConfetti.transform.localPosition = new Vector3 (-365.8f, -5.3f);
		} else if (indexPosition == 2) {
			newConfetti.transform.localPosition = new Vector3 (97.1f, 218.6f);
		} else {
			newConfetti.transform.localPosition = new Vector3 (274f, -90.5f);
		}
		
		newConfetti.GetComponent<Animator> ().SetInteger ("Index", Random.Range (1, 4));
	}
}

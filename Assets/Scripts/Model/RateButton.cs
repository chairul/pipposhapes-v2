﻿using UnityEngine;
using System.Collections;

public class RateButton : BasicButton {

	public override void Activate () {
		Application.OpenURL (DataManager.GetStoreAddressLink());
	}
}

﻿using UnityEngine;
using System.Collections;

public class RaiseBGMVolumeButton : BasicButton {

	public override void Activate () {
		SoundManager.bgm.volume = 0.7f;
	}
}

﻿using UnityEngine;
using System.Collections;

public class ParentalReminder : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CloseWindow() {
		Destroy (gameObject);
	}

	public void GoToParentalMenu() {
		ParentalMenu.specificOrder = ParentalMenu.ORDER_IAP;
		Application.LoadLevel ("Parental Menu");
	}
}

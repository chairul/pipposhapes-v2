﻿using UnityEngine;
using System.Collections;

public class BGMStateButton : TwoStateButton {

	protected override void ManageActiveState () {
		isActive = SoundManager.isBgmOn;
	}

	public override void Activate () {
		SoundManager.isBgmOn = !SoundManager.isBgmOn;

		if (SoundManager.isBgmOn) {
			SoundManager.bgm.Play();
		} else {
			SoundManager.bgm.Stop();
		}
		base.Activate ();
	}
}

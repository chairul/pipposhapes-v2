﻿using UnityEngine;
using System.Collections;

public class ExternalLinkButton : BasicButton {
	
	public string externalLink;
	
	public override void Activate () {
		Application.OpenURL (externalLink);
	}
}

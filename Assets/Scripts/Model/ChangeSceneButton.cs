﻿using UnityEngine;
using System.Collections;

public class ChangeSceneButton : BasicButton {

	public string destinationSceneName;
	public bool isHomeButton;

	public override void Activate () {
		Application.LoadLevel (destinationSceneName);
	}
	
	public override void PlaySFX() {
		if (isHomeButton) {
			SoundManager.PlaySFXOneShot (SoundManager.clipSFXHomeButton);
		} else {
			base.PlaySFX();
		}
	}
}

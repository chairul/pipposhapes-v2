﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChallengeFeedback : MonoBehaviour {

	public static string CODE_CORRECT = "correct";
	public static string CODE_WRONG = "wrong";

	public delegate void FinishDisplayingFeedback();
	public event FinishDisplayingFeedback OnFinishDisplayingFeedback;
	
	private static int recentPositiveFeedback;
	private static int recentNegativeFeedback;

	public GameObject backLayer;
	public GameObject correctFeedback;
	public GameObject wrongFeedback;
	public Text textFeedback;

	// Use this for initialization
	void Start () {
		correctFeedback.GetComponent<AnimationEvent>().OnAnimationFinished += HandleOnAnimationFinished;
		wrongFeedback.GetComponent<AnimationEvent> ().OnAnimationFinished += HandleOnAnimationFinished;
		
		recentNegativeFeedback = -1;
		recentPositiveFeedback = -1;
	}

	void HandleOnAnimationFinished (GameObject aObject) {
		aObject.SetActive (false);
		FinishFeedback ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void DisplayFeedback(string result) {
		textFeedback.enabled = false;
		backLayer.SetActive (true);

		if (result == CODE_CORRECT) {
			correctFeedback.SetActive(true);
			textFeedback.text = LocalizedContent.GetLocaleText("feedback_correct");
			
			int feedbackIndex;
			do {
				feedbackIndex = Random.Range(0, SoundManager.clipsVOPositiveFeedback.Length);
			} while (feedbackIndex == recentPositiveFeedback);

			recentPositiveFeedback = feedbackIndex;
			if (Application.loadedLevelName == "Gameplay Find") {
				SoundManager.PlaySFXOneShot(SoundManager.clipSFXFeedbackCorrect2);
			} else {
				SoundManager.PlaySFXOneShot(SoundManager.clipSFXFeedbackCorrect);
			}
			SoundManager.PlaySFXOneShot(SoundManager.clipsVOPositiveFeedback[feedbackIndex]);
		} else {
			wrongFeedback.SetActive(true);
			textFeedback.text = LocalizedContent.GetLocaleText("feedback_wrong");
			
			int feedbackIndex;
			do {
				feedbackIndex = Random.Range(0, SoundManager.clipsVONegativeFeedback.Length);
			} while (feedbackIndex == recentNegativeFeedback);
			
			recentNegativeFeedback = feedbackIndex;
			SoundManager.PlaySFXOneShot(SoundManager.clipSFXFeedbackWrong);
			SoundManager.PlaySFXOneShot(SoundManager.clipsVONegativeFeedback[feedbackIndex]);
		}

		Invoke ("DisplayTextFeedback", 0.6f);
	}

	public void FinishFeedback() {
		textFeedback.enabled = false;
		backLayer.SetActive (false);
		OnFinishDisplayingFeedback ();
	}

	public void DisplayTextFeedback() {
		textFeedback.enabled = true;
	}
}

﻿using UnityEngine;
using System.Collections;

public class ClickableObject : MonoBehaviour {

	public delegate void Click(GameObject theObject);
	public event Click OnClick;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnMouseClick() {
		if (OnClick != null) {
			OnClick(gameObject);
		}
	}
}

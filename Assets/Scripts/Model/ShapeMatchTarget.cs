﻿using UnityEngine;
using System.Collections;

public class ShapeMatchTarget : MonoBehaviour {

	public GameMatchManager controller;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D collider) {
		if (collider.gameObject.CompareTag("ShapeObject")) {
			controller.isShapeMatched = GetComponentInChildren<ShapeObject>().type == collider.transform.parent.GetComponent<ShapeObject>().type;
			controller.MeetTarget();
		}
	}

	void OnTriggerExit2D(Collider2D collider) {
		if (collider.gameObject.CompareTag("ShapeObject")) {
			controller.LeaveTarget();
		}
	}
}

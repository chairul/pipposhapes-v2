﻿using UnityEngine;
using System.Collections;

public class SendFeedbackButton : BasicButton {

	public override void Activate () {
		PlaySFX ();
		AndroidSocialGate.SendMail ("ArsaKids Feedback", "", "Pippo Shape Feedback", "support@arsakids.com");
	}
}

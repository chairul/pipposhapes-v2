﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Confetti : MonoBehaviour {

	private static List<Color> colorPalettes;

	public static void InitializeColor() {
		colorPalettes = new List<Color> ();
		colorPalettes.Add (new Color (255f/255f, 107f/255f, 107f/255f));
		colorPalettes.Add (new Color (26f/255f, 211f/255f, 180f/255f));
		colorPalettes.Add (new Color (254f/255f, 228f/255f, 79f/255f));
		colorPalettes.Add (new Color (58f/255f, 175f/255f, 203f/255f));
		colorPalettes.Add (new Color (255f/255f, 255f/255f, 255f/255f));
	}

	// Use this for initialization
	void Start () {
		if (colorPalettes == null) {
			InitializeColor();
		}

		GetComponent<Image> ().color = colorPalettes [Random.Range (0, colorPalettes.Count)];
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

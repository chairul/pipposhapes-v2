﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ArsakidsBanner : MonoBehaviour {

	public Sprite[] bannerSprites;

	private const int UPGRADE = 0;
	private const int DEV_PAGE = 1;
	private const int GAME_LINK = 2;

	private int code;
	private string[] otherGameID = {"alphabet", "shapes"};

	// Use this for initialization
	void Start () {
		code = Random.Range (0, bannerSprites.Length);

		if (code >= GAME_LINK && otherGameID[code - GAME_LINK] == DataManager.gameID) {
			do {
				code = Random.Range(GAME_LINK, bannerSprites.Length);
			} while (otherGameID[code - GAME_LINK] == DataManager.gameID);
		}

		GetComponent<Image> ().sprite = bannerSprites [code];
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClicked() {
		if (code == UPGRADE) {
			ParentalMenu.specificOrder = ParentalMenu.ORDER_IAP;
			Application.LoadLevel ("Parental Menu");
		} else if (code == DEV_PAGE) {
			Application.OpenURL ("https://play.google.com/store/apps/dev?id=5643013492782679076");
		} else if (code >= GAME_LINK) {
			Application.OpenURL (DataManager.GetStoreAddressLink(otherGameID[code - GAME_LINK]));
		}
	}
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShapeObject : DraggableObject {

	public static Sprite[] shapeTextures;

	public static void LoadShapeTextures() {
		shapeTextures = Resources.LoadAll<Sprite> ("Sprites/ShapeTexture");
	}

	public enum ShapeType {Square, Circle, Triangle, SemiCircle, Rectangle, Parallelogram}

	public delegate void Click (ShapeObject shape);
	public event Click OnClick;

	public ShapeType type;
	public Image pattern;

	public int index;

	private bool isTextureApplied;

	// Use this for initialization
	void Start () {
		if (!isTextureApplied) {
			pattern.sprite = shapeTextures [Random.Range (0, shapeTextures.Length)];
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnMouseClick() {
		if (!isDragged) {
			if (OnClick != null) {
				OnClick (this);
			}
		}
	}

	public void DisableDrag() {
		isDragDisabled = true;
	}

	public void SetTexture(int textureIndex) {
		pattern.sprite = shapeTextures [textureIndex];
		isTextureApplied = true;
	}
}

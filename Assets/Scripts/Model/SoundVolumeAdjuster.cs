﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SoundVolumeAdjuster : MonoBehaviour {

	public static float sfxVolume = 0.7f;
	public static float voVolume = 0.6f;

	public enum SoundType { BGM, SFX, VO };

	public SoundType soundType;
	public Text textVolume;

	// Use this for initialization
	void Start () {
		if (soundType == SoundType.BGM) {
			GetComponent<Slider>().value = SoundManager.bgm.volume;
			textVolume.text = "BGM : " + SoundManager.bgm.volume.ToString("0.00");
		} else if (soundType == SoundType.SFX) {
			GetComponent<Slider>().value = sfxVolume;
			textVolume.text = "SFX : " + sfxVolume.ToString("0.00");
		} else {
			GetComponent<Slider>().value = voVolume;
			textVolume.text = "VO : " + voVolume.ToString("0.00");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnVolumeChanged(float volume) {
		if (soundType == SoundType.BGM) {
			SoundManager.bgm.volume = volume;
			textVolume.text = "BGM : " + volume.ToString("0.00");
		} else if (soundType == SoundType.SFX) {
			sfxVolume = volume;
			textVolume.text = "SFX : " + volume.ToString("0.00");
		} else {
			voVolume = volume;
			textVolume.text = "VO : " + volume.ToString("0.00");
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class AnimationEvent : MonoBehaviour {

	public delegate void AnimationFinished(GameObject aObject);
	public event AnimationFinished OnAnimationFinished;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void TriggerAnimationFinished() {
		if (OnAnimationFinished != null) {
			OnAnimationFinished(gameObject);
		}
	}
}

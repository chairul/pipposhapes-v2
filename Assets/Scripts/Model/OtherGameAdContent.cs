﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OtherGameAdContent : MonoBehaviour {

	private string linkToApps;

	public Image icon;
	public Text textTitle;

	private string id;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Init(Sprite iconSprite, string title, string id) {
		icon.sprite = iconSprite;
		textTitle.text = title;
		this.id = id;
	}

	public void OpenLink() {
		// Open linkToApps
		Application.OpenURL (DataManager.GetStoreAddressLink(id));
	}
}

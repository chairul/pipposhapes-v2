﻿using UnityEngine;
using System.Collections;

public class SFXOneShot : MonoBehaviour {

	private bool hasStartedPlaying;
	private AudioSource audioSource;

	void Start () {
		DontDestroyOnLoad (gameObject);

		audioSource = GetComponent<AudioSource> ();
	}

	void Update() {
		if (hasStartedPlaying && !audioSource.isPlaying) {
			Destroy(gameObject);
		}
	}

	public void Play(AudioClip sfxClip) {
		if (SoundManager.isSfxOn) {
			if (audioSource == null) {
				audioSource = GetComponent<AudioSource>();
			}

			audioSource.clip = sfxClip;
			audioSource.Play();
		}

		hasStartedPlaying = true;
	}
}

﻿using UnityEngine;
using System.Collections;

public class LowerBGMVolumeButotn : BasicButton {
	
	public override void Activate () {
		SoundManager.bgm.volume = 0.6f;
	}
}

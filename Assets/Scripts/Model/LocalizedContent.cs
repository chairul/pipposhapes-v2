﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class LocalizedContent : MonoBehaviour {

	public enum ContentType {Text, ImageUI, ImageObject};

	private static Dictionary<string, string> localizedTextList;

	public static void LoadText() {
		localizedTextList = new Dictionary<string, string> ();
		
		var textData = JSONNode.Parse(Resources.Load<TextAsset>("Data/localized_text").text);
		string localeCode = DataManager.locale.ToString ();

		for (int i = 0; i < textData["key"].AsArray.Count; i++) {
			localizedTextList.Add(textData["key"][i], textData[localeCode][textData["key"][i]]);
		}
	}

	public static string GetLocaleText(string code) {
		return localizedTextList [code];
	}

	public ContentType type;
	public string code;

	// Use this for initialization
	void Start () {
		switch (type) {
		case ContentType.Text:
			GetComponent<Text>().text = localizedTextList[code];
			break;
		case ContentType.ImageUI:
			Sprite sprite = Resources.Load<Sprite>("Sprites/LocalizedContent/lc_" + DataManager.locale.ToString() + "_" + code);
			GetComponent<Image>().sprite = sprite;
			GetComponent<RectTransform>().sizeDelta = sprite.rect.size;
			break;
		case ContentType.ImageObject:
			break;
		default:
			break;
		}
	}
}

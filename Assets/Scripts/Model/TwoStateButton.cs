﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TwoStateButton : BasicButton {

	public Sprite spriteActive;
	public Sprite spriteInactive;

	protected bool isActive;

	void OnEnable() {
		ManageActiveState ();
		AdjustSpriteDisplay ();
	}

	protected virtual void ManageActiveState() {

	}

	private void AdjustSpriteDisplay() {
		if (isActive) {
			GetComponent<Image> ().sprite = spriteActive;
		} else {
			GetComponent<Image> ().sprite = spriteInactive;
		}
	}

	public override void Activate () {
		ManageActiveState ();
		AdjustSpriteDisplay ();
	}
}

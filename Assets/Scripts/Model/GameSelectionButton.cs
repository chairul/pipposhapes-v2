﻿using UnityEngine;
using System.Collections;

public class GameSelectionButton : BasicButton {
	
	public delegate void GameplaySelected(GameObject theObject);
	public event GameplaySelected OnGameplaySelected;

	public string gameplayName;
	public AudioClip clipVOTitle;

	void Start() {
//		GetComponent<Animator> ().SetTrigger ("Choose");
	}

	public void OnMouseUpAsButton () {
		OnGameplaySelected (gameObject);
		SoundManager.PlaySFXOneShot (clipVOTitle);
		base.PlaySFX ();
//		base.OnMouseUpAsButton ();
		Invoke ("Activate", 2f);
	}

	public override void Activate () {
		Application.LoadLevel (gameplayName);
	}
}

﻿using UnityEngine;
using System.Collections;

public class SFXStateButton : TwoStateButton {
	
	protected override void ManageActiveState () {
		isActive = SoundManager.isSfxOn;
	}
	
	public override void Activate () {
		SoundManager.isSfxOn = !SoundManager.isSfxOn;
		base.Activate ();
	}
}

﻿using UnityEngine;
using System.Collections;

public class DraggableObject : MonoBehaviour {

	public delegate void StartDrag();
	public event StartDrag OnStartDrag;

	public delegate void Drag ();
	public event Drag OnDrag;

	public delegate void FinishDrag (GameObject draggedObject);
	public event FinishDrag OnFinishDrag;

	public Animator shapeAnimator;

	protected bool isDragged;
	protected bool isDragDisabled;
	private Vector3 initialPosition;

	public void OnMouseDown() {
		initialPosition = transform.position;
		
		SoundManager.PlaySFXOneShot(SoundManager.clipSFXShapeClicked);
		if (OnStartDrag != null) {
			OnStartDrag ();
		}

		if (shapeAnimator != null && !isDragDisabled) {
			shapeAnimator.SetTrigger("Touch");
		}
	}

	public void OnMouseDrag() {
		isDragged = true;

		if (!isDragDisabled) {
			Vector3 newPosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			newPosition.z = 0;

			transform.position = newPosition;

			if (OnDrag != null) {
				OnDrag ();
			}
		}
	}

	public void OnMouseUp() {
		isDragged = false;

		if (OnFinishDrag != null && !isDragDisabled) {
			OnFinishDrag(gameObject);
		}
	}

	public void ReturnToInitialPosition() {
		transform.position = initialPosition;
	}
}

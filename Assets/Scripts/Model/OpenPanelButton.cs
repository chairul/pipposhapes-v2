﻿using UnityEngine;
using System.Collections;

public class OpenPanelButton : BasicButton {

	public GameObject panel;

	public override void Activate () {
		PlaySFX ();
		panel.SetActive (true);
	}
}

﻿using UnityEngine;
using System.Collections;

public class PurchaseInAppButton : BasicButton {

	public string productCode;

	public override void Activate () {
		InAppManager.PurchaseItem (productCode);
	}
}

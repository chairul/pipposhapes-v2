﻿using UnityEngine;
using System.Collections;

public class BasicButton : MonoBehaviour {

	protected virtual void OnMouseUpAsButton() {
		GetComponent<Animator> ().SetTrigger ("Click");
	}

	public void OnClick() {
		GetComponent<Animator> ().SetTrigger ("Click");
	}

	public void OnActivate() {
		Activate ();
		PlaySFX ();
	}

	public virtual void Activate() {

	}
	
	public virtual void PlaySFX() {
		SoundManager.PlaySFXOneShot (SoundManager.clipSFXGeneralButton);
	}
}

﻿using UnityEngine;
using System.Collections;

public class ClosePanelButton : BasicButton {
	
	public GameObject panel;
	
	public override void Activate () {
		PlaySFX ();
		panel.SetActive (false);
	}
}

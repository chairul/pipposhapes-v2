﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HO_Object : MonoBehaviour {
	public HO_Box controller;
	public float objectFill;
	public int objectIndex;
	public bool isDragable = false;
	public bool isInBox = false;
	public bool isOnBoundaries = false;
	public bool isOnOtherShape = false;
	public bool isInPlace = false;
	public bool isOnPlate = false;
	Vector2 thisPosition;

	void OnMouseDown () {
		thisPosition = this.transform.position;
		isDragable = true;
	}
	void OnMouseDrag () {
		if (isDragable) {
			Vector3 point = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			point.z = -1;
			gameObject.transform.position = point;
			Cursor.visible = false;
		}
	}
	void OnMouseUp () {
		Cursor.visible = true;
		if (!isInBox || isOnOtherShape || isOnBoundaries) {
			this.transform.position = thisPosition;
		}
		if (isOnPlate && isInPlace) {
			this.transform.position = new Vector3 (0, 0, 0);
			this.gameObject.SetActive (false);
			controller.controller.shapePlaces [objectIndex].GetComponent<Image> ().enabled = true;
			controller.controller.shapePlaces [objectIndex].SetActive (true);
			controller.RemoveObjectFromBox (objectFill);
			isInPlace = false;
			isDragable = true;
			controller.shapeDraggedCount--;
			controller.controller.placeLayout.GetComponent<RectTransform> ().sizeDelta = new Vector2 (controller.controller.placeLayoutWidth, ((controller.controller.shapePlaces.Count - controller.shapeDraggedCount) * controller.controller.shapeSize) + ((controller.controller.shapePlaces.Count - controller.shapeDraggedCount) * controller.controller.shapeSpacing));
		}
		isDragable = false;
	}

	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.tag == "Canvas") {
			isInBox = true;
		}
		if (other.gameObject.tag == "CanvasBound") {
			isOnBoundaries = true;
		}
		if (other.gameObject.tag == "Shapes") {
			isOnOtherShape = true;
		}
		if (other.gameObject.tag == "Stickable") {
			isOnPlate = true;
		}
	}
	void OnTriggerStay2D (Collider2D other) {
		if (other.gameObject.tag == "Canvas") {
			isInBox = true;
		}
		if (other.gameObject.tag == "CanvasBound") {
			isOnBoundaries = true;
		}
		if (other.gameObject.tag == "Shapes") {
			isOnOtherShape = true;
		}
		if (other.gameObject.tag == "Stickable") {
			isOnPlate = true;
		}
	}
	void OnTriggerExit2D (Collider2D other) {
		if (other.gameObject.tag == "Canvas") {
			isInBox = false;
		}
		if (other.gameObject.tag == "CanvasBound") {
			isOnBoundaries = false;
		}
		if (other.gameObject.tag == "Shapes") {
			isOnOtherShape = false;
		}
		if (other.gameObject.tag == "Stickable") {
			isOnPlate = false;
		}
	}

	void Update () {
		if (!isDragable) {
			if (isInBox && !isOnBoundaries && !isOnOtherShape && !isInPlace) {
				controller.AddObjectToBox (objectFill);
				isInPlace = true;
			}
		}
	}
}

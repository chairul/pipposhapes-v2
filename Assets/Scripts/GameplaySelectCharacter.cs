﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplaySelectCharacter : MonoBehaviour {
	public GameplaySelectorBackgroundController controller;
	public int currentNodeIndex;
	public Vector2 currentBlockLocation;
	const float xZero = -825;
	const float yZero = -265;
	const float blockSize = 50;

	float walkSpeed = 700;
	public bool isWalking = false;
	public bool isDoneStep = false;
	public int stepIndex = 1;

	[Header("To Gameplay")]
	public GameObject bubble;
	public Image gameplayButton;
	public string gameplayName;

	void OnTriggerStay2D (Collider2D other) {
		if (other.gameObject.tag == "TrackCP") {
			currentNodeIndex = other.gameObject.GetComponent<Node> ().index;
			currentBlockLocation = new Vector2 (other.gameObject.GetComponent<Node> ().blockLocation.x, other.gameObject.GetComponent<Node> ().blockLocation.y);
			controller.currentNode = currentNodeIndex;
		}
	}

	void Start () {
		Node startNode = Pathfinding.instance.allNode [GameplaySelectorBackgroundController.startNodeIndex];
		float x = xZero + (startNode.blockLocation.x * blockSize);
		float y = yZero + (startNode.blockLocation.y * blockSize);
		this.GetComponent<RectTransform> ().localPosition = new Vector3 (x, y, 0);
	}

	void Update () {
		if (isWalking) {
			if (stepIndex < controller.path.Count) {
				bubble.SetActive (false);
				float x = xZero + (controller.path[stepIndex].blockLocation.x * blockSize);
				float y = yZero + (controller.path[stepIndex].blockLocation.y * blockSize);
				this.GetComponent<RectTransform> ().localPosition = Vector2.MoveTowards (this.GetComponent<RectTransform> ().localPosition, new Vector2 (x, y), walkSpeed * Time.deltaTime);
				if (this.GetComponent<RectTransform> ().localPosition.magnitude == new Vector3 (x, y, 0).magnitude) {
					stepIndex++;
				}
			}else {
				bubble.SetActive (true);
				isWalking = false;
			}
		}
	}

	public void ToGameplay () {
		GameplaySelectorBackgroundController.startNodeIndex = controller.endNode;
		UnityEngine.SceneManagement.SceneManager.LoadScene (gameplayName);
	}
}

﻿using System.Collections;
using UnityEngine;

public class Node : MonoBehaviour {

	public int index;
	public bool isWalkable = true;
	public bool canLeft = true, canTop = true, canRight = true, canBottom = true;
	public Node prevNode;
	public Vector2 blockLocation;
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pathfinding : MonoBehaviour {
	public static Pathfinding instance;

	[SerializeField] int width, height;

	public GameObject startObj, endObj;
	public List<Node> allNode = new List<Node> ();

	int startPos, endPos;
	GameObject[] paths;

	void Awake(){
		instance = this;
		paths = new GameObject[width * height];

		int i = 0;
		int x = 0;
		int y = 0;
		foreach (Transform t in transform) {
			if (t.GetComponent<Node> () != null) {
				paths [i] = t.gameObject;
				allNode.Add (t.GetComponent<Node> ());
				t.GetComponent<Node> ().index = i;
				t.GetComponent<Node> ().blockLocation.x = x;
				t.GetComponent<Node> ().blockLocation.y = y;
				i++;
				x++;
				if (x == width) {
					x = 0;
					y++;
				}
			}
		}

		startPos = startObj.transform.GetSiblingIndex ();
		endPos = endObj.transform.GetSiblingIndex ();
	}

	void ShowMap(){
		for (int i = 0; i < paths.Length; i++) {
			if (IsWalkable (i)) {
				ChangeColor (i, Color.gray);
			} else {
				ChangeColor (i, new Color (0, 0, 0, 0));
			}
		}
	}

	void ChangeColor(int index, Color newColor){
		paths [index].GetComponent<Image> ().color = newColor;
	}

	bool IsWalkable(int index){
		return paths [index].GetComponent<Node>().isWalkable;
	}

	bool IsWalkableTo(string dir, int index){
		bool r = true;

		switch (dir) {
		case "Left":
			r = paths [index].GetComponent<Node> ().canLeft;
			break;
		case "Top":
			r = paths [index].GetComponent<Node> ().canTop;
			break;
		case "Right":
			r = paths [index].GetComponent<Node> ().canRight;
			break;
		case "Bottom":
			r = paths [index].GetComponent<Node> ().canBottom;
			break;
		}

		return r;
	}

	public List<Node> GetPath (int _from, int _to){
		Dictionary<int,bool> lastNeighbour = new Dictionary<int,bool> ();
		int startIndex = _from;
		int endIndex = _to;

		lastNeighbour.Add (startIndex, false);
		while (!lastNeighbour.ContainsKey (endIndex)) {
			List<int> curNeighbour = new List<int> ();
			for (int n = 0; n < lastNeighbour.Count; n++) {
				bool isOver = false;
				int key = GetKeyFromIndex(lastNeighbour, n);
				if (!lastNeighbour [key]) {
					lastNeighbour [key] = true;
					curNeighbour = GetNeighbour (key);
					for (int cn = 0; cn < curNeighbour.Count; cn++) {
						if (!lastNeighbour.ContainsKey (curNeighbour [cn])) {
							paths [curNeighbour [cn]].GetComponent<Node> ().prevNode = paths [key].GetComponent<Node> ();
							lastNeighbour.Add (curNeighbour [cn], false);
							if (curNeighbour [cn].Equals (endIndex)) {
								isOver = true;
								break;
							}
						}
					}
				}
				if (isOver)
					break;
			}
		}
			
		List<Node> reversedPath = new List<Node> ();
		Node lastNode = paths [endIndex].GetComponent<Node>();
		Node firstNode = paths [startIndex].GetComponent<Node>();
		Node curNode = lastNode.prevNode;
		reversedPath.Add (lastNode);
		while (!curNode.Equals(firstNode)) {
			reversedPath.Add (curNode);
			curNode = curNode.prevNode;
		}

		reversedPath.Add (firstNode);
		reversedPath.Reverse ();
		return reversedPath;
	}

	int GetKeyFromIndex(Dictionary<int,bool> dict, int index){
		int i = 0;
		int key = 0;
		foreach (int keys in dict.Keys) {
			key = keys;
			if (i == index) {
				break;
			}
			i++;
		}
		return key;
	}

	List<int> GetNeighbour(int me){
		List<int> neighbour = new List<int> ();

		// Left neighbour
		if (me % width > 0) {
			if (IsWalkable (me - 1) && IsWalkableTo ("Left", me)) {
				neighbour.Add (me - 1);
			}
		}
		// Top neighbour
		if (me < (width * height) - width) {
			if (IsWalkable (me + width) && IsWalkableTo ("Top", me)) {
				neighbour.Add (me + width);
			}
		}
		// Right neighbour
		if ((me + 1) % width > 0) {
			if (IsWalkable (me + 1) && IsWalkableTo ("Right", me)) {
				neighbour.Add (me + 1);
			}
		}
		// Bottom neighbour
		if (me > width - 1) {
			if (IsWalkable (me - width) && IsWalkableTo ("Bottom", me)) {
				neighbour.Add (me - width);
			}
		}

		return neighbour;
	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.M)) {
			ShowMap ();
		}
	}
}

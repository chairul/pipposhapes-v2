﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoringEraser : MonoBehaviour {
	public ColoringDrawingController controller;
	public GameObject lineSample;
	//TESTTTTT

	//Setup
	public LineRenderer line;
	public Vector2 leftPointPosition;
	public Vector2 rightPointPosition;
	//Result
	public int leftIndex = 0;
	public int rightIndex = 0;

	void OnCollisionStay2D (Collision2D other) {
		if (other.gameObject.tag == "Line") {
			ContactPoint2D[] cps = other.contacts;
			List<ContactPoint2D> cpRight = new List<ContactPoint2D> ();
			List<ContactPoint2D> cpLeft = new List<ContactPoint2D> ();

			if (cps.Length > 1) {
				for (int i = 0; i < cps.Length; i++) {
					if (cps [i].point.x > this.transform.position.x) {
						cpRight.Add (cps [i]);
					} else {
						cpLeft.Add (cps [i]);
					}
				}
				if (cpRight.Count > cpLeft.Count) {
					for (int i = 0; i < cpLeft.Count; i++) {
						Debug.DrawLine (cpLeft [i].point, cpRight [i].point, Color.green, 20f);
					}
				} else {
					for (int i = 0; i < cpRight.Count; i++) {
						Debug.DrawLine (cpRight [i].point, cpLeft [i].point, Color.green, 20f);
					}
				}
			}
			if (cpLeft.Count > 0 && cpRight.Count > 0) {
				leftPointPosition = cpLeft [0].point;
				rightPointPosition = cpRight [0].point;
			}
			line = other.gameObject.GetComponent<LineRenderer> ();
		}
	}


	int FindNearesLinePoint (Vector2 point, int code) {
		int index = 0;
		Vector2[] linePoints = new Vector2[line.positionCount];
		for (int i = 0; i < linePoints.Length; i++) {
			linePoints [i] = line.GetPosition (i);
		}

		float[] diff = new float[linePoints.Length];
		for (int i = 0; i < linePoints.Length; i++) {
			diff [i] = Mathf.Abs (Vector2.Distance (point, linePoints [i]));
		}

		float min = Mathf.Infinity;
		if (code == 0) {
			for (int i = 0; i < diff.Length; i++) {
				if (diff [i] < min) {
					if (linePoints [i].magnitude < point.magnitude) {
						min = diff [i];
						index = i;
					}
				}
			}
		} else {
			for (int i = 0; i < diff.Length; i++) {
				if (diff [i] < min) {
					if (linePoints [i].magnitude > point.magnitude) {
						min = diff [i];
						index = i;
					}
				}
			}
		}
		Debug.Log (point.x + " -- " + index);
		return index;
	}

	void DivideLine (LineRenderer originalLine) {
		GameObject newLineLeft = Instantiate (lineSample);
		newLineLeft.SetActive (true);
		newLineLeft.GetComponent<LineRenderer> ().sortingOrder = originalLine.sortingOrder + 1;
		newLineLeft.GetComponent<LineRenderer> ().startWidth = originalLine.startWidth;
		newLineLeft.GetComponent<LineRenderer> ().endWidth = originalLine.endWidth;
		newLineLeft.GetComponent<LineRenderer> ().startColor = Color.red;
		newLineLeft.GetComponent<LineRenderer> ().endColor = Color.red;
		newLineLeft.GetComponent<LineRenderer> ().positionCount = leftIndex;
		for (int i = 0; i < leftIndex; i++) {
			newLineLeft.GetComponent<LineRenderer> ().SetPosition (i, originalLine.GetPosition(i));
		}


		GameObject newLineRight = Instantiate (lineSample);
		newLineRight.SetActive (true);
		newLineRight.GetComponent<LineRenderer> ().sortingOrder = originalLine.sortingOrder + 1;
		newLineRight.GetComponent<LineRenderer> ().startWidth = originalLine.startWidth;
		newLineRight.GetComponent<LineRenderer> ().endWidth = originalLine.endWidth;
		newLineRight.GetComponent<LineRenderer> ().startColor = Color.blue;
		newLineRight.GetComponent<LineRenderer> ().endColor = Color.blue;
		newLineRight.GetComponent<LineRenderer> ().positionCount = originalLine.positionCount - rightIndex;
		for (int i = 0; i < originalLine.positionCount - rightIndex; i++) {
			newLineRight.GetComponent<LineRenderer> ().SetPosition (i, originalLine.GetPosition (i + rightIndex));
		}
	}

	void Update () {
		if (Input.GetKeyDown (KeyCode.L)) {
			leftIndex = FindNearesLinePoint (leftPointPosition, 0);
			rightIndex = FindNearesLinePoint (rightPointPosition, 1);
		}
		if (Input.GetKeyDown(KeyCode.P)) {
			DivideLine (line);
		}
	}
}

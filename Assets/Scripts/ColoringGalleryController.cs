﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
public class ColoringGalleryController : MonoBehaviour {
	public ColoringGameController gameController;
	public Image[] frame;
	public GameObject fullFrame;
	public Image fullFrameImage;

	void Start () {
		gameController = this.GetComponent<ColoringGameController> ();
		StartGallery ();
	}

	public void StartGallery () {
		for (int i = 0; i < frame.Length; i++) {
			SetGalleryFor (i);
		}
	}

	void SetGalleryFor (int frameIndex) {
		if (!File.Exists (Application.persistentDataPath + " " + frameIndex + ".png")) {
			frame [frameIndex].sprite = null;
			frame [frameIndex].gameObject.GetComponent<Button> ().interactable = false;
		} else {
			var bytes = File.ReadAllBytes (Application.persistentDataPath + " " + frameIndex + ".png");
			Texture2D tex = new Texture2D (1024, 768);
			tex.LoadImage (bytes);
			Sprite newSprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0, 0));
			frame [frameIndex].sprite = newSprite;
			frame [frameIndex].gameObject.GetComponent<Button> ().interactable = true;
		}
	}

	public void FullScreenGallery (int frameIndex) {
		var bytes = File.ReadAllBytes (Application.persistentDataPath + " " + frameIndex + ".png");
		Texture2D tex = new Texture2D (1024, 768);
		tex.LoadImage (bytes);
		Sprite newSprite = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), new Vector2 (0, 0));
		fullFrameImage.sprite = newSprite;
		fullFrame.SetActive (true);
	}
	public void CloseFullScreen () {
		fullFrame.SetActive (false);
	}
	public void DeleteGallery (int frameIndex) {
		fullFrame.SetActive (false);
		File.Delete (Application.persistentDataPath + " " + frameIndex + ".png");
		StartGallery ();
	}

	public void BackToMain () {
		gameController.background.sprite = gameController.backgrounds [0];
		gameController.mainScreen.SetActive (true);
		gameController.galleryScreen.SetActive (false);
	}


	//test delete
	void Update () {
		if (Input.GetKeyDown(KeyCode.P)) {
			DeleteGallery (0);
		}
		if (Input.GetKeyDown(KeyCode.O)) {
			DeleteGallery (1);
		}
		if (Input.GetKeyDown(KeyCode.I)) {
			DeleteGallery (2);
		}
	}
}

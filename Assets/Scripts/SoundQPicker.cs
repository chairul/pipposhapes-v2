﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundQPicker : MonoBehaviour {
	public List<int> shapesID = new List<int>();
	public int currentQuiz;
	public bool isAnswered = false;
	public int answerCode = 0;
	public int rightAnswered = 0;
	public int wrongAnswered = 0;
	float delayNextQuiz = 2f;
	//previewing
	public List<Button> answerButtons = new List<Button> ();
	public Text rightScore;
	public Text worngScore;
	public ChallengeFeedback feedback;

	//indicator
	public GameObject indicator;
	public GameObject result;

	void Start () {
		SetQuiz ();
	}

	void SetQuiz () {
		GetShapes ();
		GenerateQuiz ();
		GenerateAnswers ();

		indicator.SetActive (false);

		for (int i = 0; i < answerButtons.Count; i++) {
			answerButtons [i].interactable = true;
		}
		delayNextQuiz = 2;
		isAnswered = false;
	}

	void GetShapes () {
		shapesID.Clear ();
		//Get All Shapes (ID)
		Sprite[] shapes = Resources.LoadAll<Sprite> ("Shapes");
		for (int i = 0; i < shapes.Length; i++) {
			shapesID.Add (int.Parse (shapes [i].name));
		}
	}

	void GenerateQuiz () {
		//Generate Random Shape (ID)
		currentQuiz = shapesID [Random.Range (0, shapesID.Count)];
		shapesID.Remove (currentQuiz);
		this.GetComponent<AudioSource> ().clip = Resources.Load<AudioClip> ("Shapes/Clip/" + currentQuiz);
		this.GetComponent<AudioSource> ().Play ();
	}

	void GenerateAnswers () {
		for (int i = 0; i < answerButtons.Count; i++) {
			answerButtons [i].gameObject.transform.GetChild (0).gameObject.GetComponent<Image> ().sprite = null;
		}
		int rightAnswer = Random.Range (0, answerButtons.Count);
		answerButtons [rightAnswer].gameObject.transform.GetChild (0).gameObject.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Shapes/" + currentQuiz);
		answerButtons [rightAnswer].GetComponent<SoundQAnswer> ().answer = currentQuiz;
		for (int i = 0; i < answerButtons.Count; i++) {
			answerButtons [i].GetComponent<SoundQAnswer> ().picker = this;
			if (answerButtons[i].gameObject.transform.GetChild(0).gameObject.GetComponent<Image>().sprite == null) {
				int wrongAnswer = shapesID [Random.Range (0, shapesID.Count)];
				answerButtons [i].gameObject.transform.GetChild(0).gameObject.GetComponent<Image> ().sprite = Resources.Load<Sprite> ("Shapes/" + wrongAnswer);
				answerButtons [i].GetComponent<SoundQAnswer> ().answer = wrongAnswer;
			}
		}
	}

	public void PlayShapeSound () {
		this.GetComponent<AudioSource> ().clip = Resources.Load<AudioClip> ("Shapes/Clip/" + currentQuiz);
		this.GetComponent<AudioSource> ().Play ();
	}

	void Update () {
		if (isAnswered) {
			for (int i = 0; i < answerButtons.Count; i++) {
				answerButtons [i].interactable = false;
			}
			delayNextQuiz -= Time.deltaTime;
			if (delayNextQuiz <= 0) {
				if (rightAnswered < 5) {
					if (answerCode == 1) {
						SetQuiz ();
					} else {
						indicator.SetActive (false);

						for (int i = 0; i < answerButtons.Count; i++) {
							answerButtons [i].interactable = true;
						}
						delayNextQuiz = 2;
						isAnswered = false;
					}
				} else {
					result.SetActive (true);
				}
			}
		}
	}
}

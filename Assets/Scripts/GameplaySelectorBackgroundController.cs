﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplaySelectorBackgroundController : MonoBehaviour {
	[Header("Layouts")]
	public GameObject background;
	public Sprite[] backgrounds;
	public GameObject clouds;
	public GameObject stars;
	public int skyCode = 0;

	[Header("Pathfinder")]
	public GameplaySelectCharacter character;
	public static int startNodeIndex;
	public List<Node> path = new List<Node> ();
	public int currentNode;
	public int endNode;

	[Header("To Gameplay")]
	public int targetGameplay = 0;
	public string[] gameplayNames;
	public Sprite[] gameplayButtons;

	void ChangeSky (int code) {
		background.GetComponent<Image> ().sprite = backgrounds [code];
		if (code == 0 || code == 1) {
			clouds.SetActive (true);
			stars.SetActive (false);
		} else {
			clouds.SetActive (false);
			stars.SetActive (true);
		}
	}

	void Start () {
		ChangeSky (skyCode);
		currentNode = startNodeIndex;
	}

	public void GetButtonNode (int node) {
		if (currentNode != node) {
			endNode = node;
			path.Clear ();
			path = Pathfinding.instance.GetPath (currentNode, endNode);
			character.isWalking = true;
			character.stepIndex = 1;
		} else {
			endNode = node;
			path.Clear ();
			character.bubble.SetActive (true);
		}
	}
	public void SetTargetGamplay (int target) {
		targetGameplay = target;
		character.gameplayButton.sprite = gameplayButtons [target];
		character.gameplayName = gameplayNames [target];
	}

	void Update () {
		if (character.isWalking) {
			for (int i = 0; i < Pathfinding.instance.allNode.Count; i++) {
				Pathfinding.instance.allNode [i].gameObject.GetComponent<BoxCollider2D> ().enabled = true;
			}
		} else {
			for (int i = 0; i < Pathfinding.instance.allNode.Count; i++) {
				Pathfinding.instance.allNode [i].gameObject.GetComponent<BoxCollider2D> ().enabled = false;
			}
		}
			


		if (Input.GetKeyDown(KeyCode.Q)) {
			ChangeSky (0);
		}
		if (Input.GetKeyDown(KeyCode.W)) {
			ChangeSky (1);
		}
		if (Input.GetKeyDown(KeyCode.E)) {
			ChangeSky (2);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;

public class FlashCardController : MonoBehaviour {
	public List<int> cardsID;
	public int currentCardID;
	public string cardsData;
	JSONNode load;
	//previewing
	public Image cardShape;
	public Text cardShapeName;
	public AudioClip cardShapeSound;
	public Image[] borders;

	void Awake () {
		//get all sprite shapes from shape folder
		Sprite[] cards = Resources.LoadAll<Sprite> ("Shapes");
		foreach (Sprite item in cards) {
			cardsID.Add (int.Parse (item.name));
		}
		//load cards data
		cardsData = Resources.Load<TextAsset> ("Data/flashcards").ToString ();
	}

	void Start () {
		//load random shape card
		int randomCard = 0;
		currentCardID = cardsID [randomCard];
		LoadCard (currentCardID);
	}

	void LoadCard (int cardID) {
		//parse card data
		load = JSON.Parse (cardsData);

		//change shape's image in card
		cardShape.sprite = Resources.Load<Sprite> ("Shapes/" + cardID);
		//change shape's name in card
		cardShapeName.text = load [cardID] ["name"];
		//change shape's sound in card
		cardShapeSound = Resources.Load<AudioClip> ("ShapeSample/Clip/" + cardID);
		this.GetComponent<AudioSource> ().clip = cardShapeSound;

		//set borders
		Color temp;
		//2 prev borders
		ColorUtility.TryParseHtmlString (load [Modulo ((cardID - 2), cardsID.Count)] ["groupColor"], out temp);
		borders [0].color = temp;
		ColorUtility.TryParseHtmlString (load [Modulo ((cardID - 1), cardsID.Count)] ["groupColor"], out temp);
		borders [1].color = temp;
		//this card border
		ColorUtility.TryParseHtmlString (load [cardID] ["groupColor"], out temp);
		borders [2].color = temp;
		//2 next borders
		ColorUtility.TryParseHtmlString (load [Modulo ((cardID + 1), cardsID.Count)] ["groupColor"], out temp);
		borders [3].color = temp;
		ColorUtility.TryParseHtmlString (load [Modulo ((cardID + 2), cardsID.Count)] ["groupColor"], out temp);
		borders [4].color = temp;
	}

	public void NextCard () {
		currentCardID = Modulo ((currentCardID + 1), cardsID.Count);
		LoadCard (currentCardID);
	}
	public void PrevCard () {
		currentCardID = Modulo ((currentCardID - 1), cardsID.Count);
		LoadCard (currentCardID);
	}
	public void RandomCard () {
		int randomCardID = Random.Range (0, cardsID.Count);
		int currentGroupCode = load [currentCardID] ["groupCode"].AsInt;
		while (load [randomCardID] ["groupCode"].AsInt != currentGroupCode || currentCardID == randomCardID) {
			randomCardID = Random.Range (0, cardsID.Count);
		}
		currentCardID = randomCardID;
		LoadCard (currentCardID);
	}
	public void Say () {
		this.GetComponent<AudioSource> ().Play ();
	}

	int Modulo (int a, int b) {
		return (a % b + b) % b;
	}
}

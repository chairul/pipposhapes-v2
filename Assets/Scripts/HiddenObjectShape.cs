﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HiddenObjectShape : MonoBehaviour {
	public bool isTracking = false;
	public HiddenObjectCheckPoint[] CPs;
	public GameObject[] TRs;
	public GameObject currentCP;
	public GameObject currentTR;
	public int trackingIndex;
	public int completedCount = 0;
	public bool trackingComplete = false;

	RaycastHit2D hit;
	public GameObject lineSample;
	LineRenderer line;
	bool isMousePressed = false;
	List<Vector3> pointsList;
	Vector3 mousePos;

	void DrawLine(){
		line = Instantiate (lineSample).GetComponent<LineRenderer> ();
		line.gameObject.SetActive (true);
		line.startWidth = 0.1f;
		line.endWidth = 0.1f;
		line.startColor = Color.black;
		line.endColor = Color.black;
		line.sortingOrder = 3;
		line.positionCount = 0;
		line.useWorldSpace = true;
		isMousePressed = false;
		pointsList = new List<Vector3>();
	}
	void Update () {
		if (this.completedCount == this.CPs.Length) {
			for (int i = 0; i < CPs.Length; i++) {
				this.GetComponent<SpriteRenderer> ().color = Color.green;
			}
			Debug.Log ("DONE");
		}
		if (!isMousePressed) {
			hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
			if (hit.collider != null) {
				if (hit.collider.gameObject.tag == "TrackCP") {	
					currentCP = hit.collider.gameObject;
					isTracking = true;
				}
			} else {
				isTracking = false;
			}
		}

		if (isTracking) {
			if (Input.GetMouseButtonDown (0)) {
				DrawLine ();
				isMousePressed = true;
			}
			if (Input.GetMouseButtonUp(0)) {
				isMousePressed = false;
				isTracking = false;
			}
			if (isMousePressed) {
				for (int i = 0; i < currentCP.GetComponent<HiddenObjectCheckPoint> ().connectedTracks.Length; i++) {
					currentCP.GetComponent<HiddenObjectCheckPoint> ().connectedTracks [i].SetActive (true);
				}
				hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
				if (hit.collider != null) {
					if (hit.collider.gameObject.tag == "Track") {
						trackingComplete = false;
						currentTR = hit.collider.gameObject;
						for (int i = 0; i < currentCP.GetComponent<HiddenObjectCheckPoint> ().connectedTracks.Length; i++) {
							if (currentCP.GetComponent<HiddenObjectCheckPoint> ().connectedTracks [i] != currentTR) {
								currentCP.GetComponent<HiddenObjectCheckPoint> ().connectedTracks [i].SetActive (false);
							} else {
								trackingIndex = i;
							}
						}
						mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
						mousePos.z = -2;
						if (!pointsList.Contains (mousePos)) {
							pointsList.Add (mousePos);
							line.positionCount = pointsList.Count;
							line.SetPosition (pointsList.Count - 1, (Vector3)pointsList [pointsList.Count - 1]);
						}
					}
					if (hit.collider.gameObject == currentCP.GetComponent<HiddenObjectCheckPoint> ().targetCheckpoints [trackingIndex]) {
						currentTR.GetComponent<BoxCollider2D> ().enabled = false;
						completedCount++;
						line.startColor = Color.green;
						line.endColor = Color.green;
						trackingComplete = true;
						isMousePressed = false;
					}
				} else {
					isMousePressed = false;
					for (int i = 0; i < currentCP.GetComponent<HiddenObjectCheckPoint> ().connectedTracks.Length; i++) {
						currentCP.GetComponent<HiddenObjectCheckPoint> ().connectedTracks [i].SetActive (false);
					}
					line.startColor = Color.red;
					line.endColor = Color.red;
				}
			} else {
				for (int i = 0; i < currentCP.GetComponent<HiddenObjectCheckPoint> ().connectedTracks.Length; i++) {
					currentCP.GetComponent<HiddenObjectCheckPoint> ().connectedTracks [i].SetActive (false);
				}
				if (!trackingComplete) {
					Destroy (line);
				}
			}
		}
	}
}

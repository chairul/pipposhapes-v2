﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sticker : MonoBehaviour {
	public StickerController controller;
	public int stickerID;
	public bool isDragable = true;
	public bool isMatch = false;
	public GameObject targetObject;
	public Vector2 targetPosition;
	public Quaternion targetRotation;
	Vector2 thisPosition;

	void OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.tag == "Stickable") {
			if (other.gameObject.GetComponent<Stickable> ().shapeID == this.stickerID) {
				isMatch = true;
				isDragable = false;
				targetObject = other.gameObject;
				targetPosition = new Vector2 (other.transform.position.x, other.transform.position.y);
				targetRotation = new Quaternion (other.transform.rotation.x, other.transform.rotation.y, other.transform.rotation.z, other.transform.rotation.w);
			} else {
				isMatch = false;
			}
		}
	}
	void OnTriggerExit2D (Collider2D other){
		if (other.gameObject.tag == "Stickable") {
			if (other.gameObject.GetComponent<Stickable> ().shapeID == this.stickerID) {
				isMatch = false;
			}
		}
	}
}

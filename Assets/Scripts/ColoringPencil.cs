﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoringPencil : MonoBehaviour {
	public ColoringDrawingController controller;
	public bool isDrawing = false;

	public GameObject lineSample;
	LineRenderer line;
	bool isMousePressed = false;
	List<Vector3> pointsList;
	Vector3 mousePos;

	//Track Points
	public Vector2[] points;
	public Vector2[] pointsUp;
	public Vector2[] pointsDown;
	float h = 0.1f;


	void DrawLine(){
		line = Instantiate (lineSample).GetComponent<LineRenderer> ();
		line.gameObject.SetActive (true);
		line.startWidth = 0.1f;
		line.endWidth = 0.1f;
		line.startColor = controller.currentPencil;
		line.endColor = controller.currentPencil;
		line.sortingOrder = controller.currentLayer;
		controller.currentLayer++;
		line.positionCount = 0;
		line.useWorldSpace = true;
		isMousePressed = false;
		pointsList = new List<Vector3>();
	}

	void Update () {
		if (controller.isInPencilMode) {
			RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
			if (hit.collider != null) {
				if (hit.collider.gameObject.tag == "Canvas" || hit.collider.gameObject.tag == "Stickable" || hit.collider.gameObject.tag == "Line") {
					isDrawing = true;
				} else {
					isDrawing = false;
				}
			} else {
				isDrawing = false;
			}

			if (isDrawing) {
				if (Input.GetMouseButtonDown (0)) {
					DrawLine ();
					isMousePressed = true;
				}
				if (Input.GetMouseButtonUp (0)) {
					isMousePressed = false;
					if (line.positionCount <= 1) {
						Destroy (line.gameObject);
					} else {
						SetPoly ();
					}
				}
				if (isMousePressed) {
					mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
					mousePos.z = -2;
					if (!pointsList.Contains (mousePos)) {
						pointsList.Add (mousePos);
						line.positionCount = pointsList.Count;
						line.SetPosition (pointsList.Count - 1, (Vector3)pointsList [pointsList.Count - 1]);
					}
				}
			} else {
				isMousePressed = false;
				if (line != null && line.positionCount > 1 && line.GetComponent<PolygonCollider2D> () == null) {
					SetPoly ();
				}
			}
		}
	}

	void SetPoly () {
		line.gameObject.AddComponent<PolygonCollider2D> ();

		//get points from line
		points = new Vector2[pointsList.Count];
		for (int i = 0; i < points.Length; i++) {
			points [i].x = pointsList [i].x;
			points [i].y = pointsList [i].y;
		}

		//for first point
		pointsUp = new Vector2[points.Length];
		pointsDown = new Vector2[points.Length];

		//for points
		if (points.Length > 1) {
			FindUpDownPoints (0, points [0], points [1]);
			for (int i = 0; i < points.Length; i++) {
				if (i > 0) {
					FindUpDownPoints (i, points [i], points [i - 1]);
				}
			}
		}

		//merge 2 vertor2 arrays
		Vector2[] pointsFix = new Vector2[pointsUp.Length + pointsDown.Length];
		int temp = pointsDown.Length - 1;
		for (int i = 0; i < pointsFix.Length; i++) {
			if (i >= pointsUp.Length) {
				pointsFix [i] = pointsDown [temp];
				temp--;
			} else {
				pointsFix [i] = pointsUp [i];
			}
		}

		//build polygon collider
		line.GetComponent<PolygonCollider2D> ().SetPath (0, pointsFix);
	}

	void FindUpDownPoints (int index, Vector2 currentPoint, Vector2 prevPoint) {
		Vector2 v = currentPoint - prevPoint;
		if (index == 0) {
			v = prevPoint - currentPoint;
		}
		float P3x = -v.y / Mathf.Sqrt (Mathf.Pow (v.x, 2) + Mathf.Pow (v.y, 2)) * h;
		float P3y = v.x / Mathf.Sqrt (Mathf.Pow (v.x, 2) + Mathf.Pow (v.y, 2)) * h;
		float P4x = -v.y / Mathf.Sqrt (Mathf.Pow (v.x, 2) + Mathf.Pow (v.y, 2)) * -h;
		float P4y = v.x / Mathf.Sqrt (Mathf.Pow (v.x, 2) + Mathf.Pow (v.y, 2)) * -h;
		pointsUp [index] = new Vector2 (currentPoint.x + P3x, currentPoint.y + P3y);
		pointsDown [index] = new Vector2 (currentPoint.x + P4x, currentPoint.y + P4y);
	}
}
